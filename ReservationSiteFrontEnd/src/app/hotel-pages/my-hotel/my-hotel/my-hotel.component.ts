import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HotelSerivceService } from '../../hotel-services/hotel-serivce.service';
import { Accomodation } from '../../models/hotel.model';
import { Room } from '../../models/room.model';

@Component({
  selector: 'app-my-hotel',
  templateUrl: './my-hotel.component.html',
  styleUrls: ['./my-hotel.component.scss']
})
export class MyHotelComponent implements OnInit {

  accomodationId: number;
  accomodation: Accomodation;
  rooms: Room[];

  constructor(private hotelService: HotelSerivceService, private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit(): void {
    this.accomodationId = parseInt(this.route.snapshot.paramMap.get("id"));
    this.getAccomodationById(this.accomodationId);
    this.getRoomsOfAccomodation(this.accomodationId);
  }

  getAccomodationById(id: number){
    return this.hotelService.getAccomodationById(id).subscribe(result => {this.accomodation = result; console.log(this.accomodation);});
  }

  getRoomsOfAccomodation(id: number){
    return this.hotelService.getRoomsOfAccomodation(id).subscribe(async result => {
      this.rooms = result;
      console.log(this.rooms);});
  }

  deleteRoom(id: number) {
    this.hotelService.deleteRoom(id);
  }

  gotoAddRoom(){
    this.router.navigate(["hotel/" + this.accomodationId + "/add-room"]);
  }



}
