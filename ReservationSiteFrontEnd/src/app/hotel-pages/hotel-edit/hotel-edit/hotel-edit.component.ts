import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { isUndefined } from 'util';
import { HotelSerivceService } from '../../hotel-services/hotel-serivce.service';
import { Accomodation } from '../../models/hotel.model';

@Component({
  selector: 'app-hotel-edit',
  templateUrl: './hotel-edit.component.html',
  styleUrls: ['./hotel-edit.component.scss']
})
export class HotelEditComponent implements OnInit {

  success = true;
  accomodationRegistrationForm: FormGroup;
  accomodation: Accomodation;
  id: number;

  constructor(private fb: FormBuilder, private http: HttpClient, private router: Router, private route: ActivatedRoute, private hotelService: HotelSerivceService) {
    this.id = parseInt(this.route.snapshot.paramMap.get("accomodationId"));
  }

  ngOnInit(): void {
    this.hotelService.getAccomodationById(this.id).subscribe(accomodation => this.accomodation = accomodation); 

    this.accomodationRegistrationForm = this.fb.group({
      name: ['',
      ],  
      city: ['',
      ],
      type: ['',
      ],
      description: ['',
      ],
      catering: ['',
      ],
      prepayment: ['',
      ],
      cancellationFee: ['',
      ],
    });
    this.accomodationRegistrationForm.valueChanges.subscribe(console.log);
  }

  public submit(){
    
    const accomodationDto = {
        "AccomodationName": (this.name.value === '') ? this.accomodation.accomodationName : this.name.value,
        "City": (this.city.value === '') ? this.accomodation.city : this.city.value,
        "Type": (this.type.value === '') ? this.accomodation.type : this.type.value,
        "Description": (this.description.value === '') ? this.accomodation.description : this.description.value,
        "OfferedCatering": (this.catering.value === '') ? this.accomodation.offeredCatering : this.catering.value,
        "PrePayment": (this.prepayment.value === '') ? this.accomodation.prePayment : parseFloat(this.prepayment.value),
        "CancellationFee": (this.cancellationFee.value === '') ? this.accomodation.cancellationFee : parseFloat(this.cancellationFee.value),
      }

      this.http.put("/api/Accomodation/edit?id=" + this.id, accomodationDto).subscribe(result =>{
        this.router.navigate(["my-hotels"]);
      }, error => {
        this.accomodationRegistrationForm.reset();
        this.accomodationRegistrationForm.setValidators(null);
        this.success = false;
      });
  }

    //Getters
    get name(){
      return this.accomodationRegistrationForm.get('name');
    }
  
    get city(){
      return this.accomodationRegistrationForm.get('city');
    }
  
    get type(){
      return this.accomodationRegistrationForm.get('type');
    }

    get description(){
      return this.accomodationRegistrationForm.get('description');
    }
  
    get catering(){
      return this.accomodationRegistrationForm.get('catering');
    }
  
    get prepayment(){
      return this.accomodationRegistrationForm.get('prepayment');
    }

    get cancellationFee(){
      return this.accomodationRegistrationForm.get('cancellationFee');
    }

}
