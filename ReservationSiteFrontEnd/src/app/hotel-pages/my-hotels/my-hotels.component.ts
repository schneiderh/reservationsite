import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HotelSerivceService } from '../hotel-services/hotel-serivce.service';
import { Accomodation } from '../models/hotel.model';

@Component({
  selector: 'app-my-hotels',
  templateUrl: './my-hotels.component.html',
  styleUrls: ['./my-hotels.component.scss']
})
export class MyHotelsComponent implements OnInit {

  id = localStorage.getItem("id");
  hotels: Accomodation[];

  constructor(private hotelService: HotelSerivceService, private router: Router) { }


  ngOnInit(): void {
    this.getMyAccomodations(parseInt(this.id));
  }

  getMyAccomodations(id: number)
  {
    this.hotelService.getMyAccomodations(id).subscribe( accomodation => {this.hotels = accomodation; console.log(this.hotels);});
  }

  editHotel(id: number) {
    this.router.navigate(['edit-hotel', { accomodationId: id }]);
  }

  inactivateHotel(id: number) {
    this.hotelService.inactivateAccomodation(id);
  }

  activateHotel(id: number) {
    this.hotelService.activateAccomodation(id);
  }

  deleteHotel(id: number) {
    this.hotelService.deleteAccomodation(id);
  }

  gotoGuests(id: number) {
    this.router.navigate(['hotel-guests/' + id]);
  }
}
