import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { HotelSerivceService } from '../../hotel-services/hotel-serivce.service';

@Component({
  selector: 'app-edit-room',
  templateUrl: './edit-room.component.html',
  styleUrls: ['./edit-room.component.scss']
})
export class EditRoomComponent implements OnInit {

  room;
  id: number;
  roomEditForm: FormGroup;
  selectedFile: File = null;

  constructor(private fb: FormBuilder,
    private http: HttpClient,
    private hotelService: HotelSerivceService,
    private route: ActivatedRoute,
    private router: Router) {
    this.id = parseInt(this.route.snapshot.paramMap.get("id"));
  }

  ngOnInit(): void {
    this.getRoomById(this.id);

    this.roomEditForm = this.fb.group({
      roomName: ['',
      ],  
      type: ['',
      ],
      maxPersons: ['',
      ],
      pricePerPerson: ['',
      ],
      image: ['',
      ],
    });
    this.roomEditForm.valueChanges.subscribe(console.log); 
  }

  getRoomById(id: number){
    this.hotelService.getRoomById(id).subscribe(result => this.room = result);
  }

  onFileSelected(event){
    this.selectedFile = <File>event.target.files[0];
  }

  submit(){
    const roomFormData = new FormData();
    roomFormData.append("RoomName", (this.roomName.value === '') ? this.room.roomName : this.roomName.value);
    roomFormData.append("RoomType", (this.type.value === '') ? this.room.roomType : this.type.value);
    roomFormData.append("MaxPersons", (this.maxPersons.value === '') ? this.room.maxPersons : parseInt(this.maxPersons.value));
    roomFormData.append("PricePerPerson", (this.pricePerPerson.value === '') ? this.room.pricePerPerson : parseInt(this.pricePerPerson.value));

    var withImage = false;
    if(this.selectedFile != null){
      withImage = true;
      roomFormData.append('Image', this.selectedFile, this.selectedFile.name);
    }

    console.log(roomFormData, withImage);

    this.http.put("/api/Room/edit?id=" + this.id + "&withImage=" + withImage, roomFormData).subscribe(result =>{
      this.router.navigate(["room/" + this.id]);
    }, error => {
      this.roomEditForm.reset();
      this.roomEditForm.setValidators(null);
    });
  }

  //Getters
  get roomName(){
    return this.roomEditForm.get('roomName');
  }

  get type(){
    return this.roomEditForm.get('type');
  }

  get maxPersons(){
    return this.roomEditForm.get('maxPersons');
  }

  get pricePerPerson(){
    return this.roomEditForm.get('pricePerPerson');
  }

}
