import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HotelSerivceService } from '../../hotel-services/hotel-serivce.service';
import { Room } from '../../models/room.model';
import {FormGroup, FormControl, FormBuilder, Validators} from '@angular/forms';
import { DatePipe } from '@angular/common';
import { DateAdapter } from '@angular/material/core';
import { exit } from 'process';
import { HttpClient } from '@angular/common/http';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.scss']
})
export class RoomComponent implements OnInit {

  id:number;
  room;
  minDate = new Date();
    year = this.minDate.getFullYear();
    month = this.minDate.getMonth();
    day = this.minDate.getDate();
  maxDate = new Date(this.year + 1, this.month, this.day);
  reservedDates: string[];
  myFilter = (d: Date | null): boolean => {
    return !this.reservedDates.includes(this.datepipe.transform(d, 'yyyy-MM-dd')) && d > new Date(this.year, this.month, this.day - 1);
  }

  roomEditForm: FormGroup;
  unlockForm: FormGroup;

  constructor(private hotelService: HotelSerivceService,
    private route: ActivatedRoute,
    private datepipe: DatePipe,
    private fb: FormBuilder,
    private http: HttpClient,
    private router: Router) {


    }

  ngOnInit(): void {
    this.id = parseInt(this.route.snapshot.paramMap.get("id"));
    this.getRoomById(this.id);
    this.getReservedDatesForRoom(this.id);

    this.roomEditForm = this.fb.group({
      lockStart: ['',[
        Validators.required
      ]],
      lockEnd: ['', [
        Validators.required
      ]],
    });
    
    this.roomEditForm.valueChanges.subscribe(console.log);

    this.unlockForm = this.fb.group({
      unlockStart: ['',[
        Validators.required
      ]],
      unlockEnd: ['', [
        Validators.required
      ]],
    });

    this.unlockForm.valueChanges.subscribe(console.log);

  }

  getRoomById(id:number){
    this.hotelService.getRoomById(id).subscribe(result => this.room = result);
  }

  getReservedDatesForRoom(id:number){
    this.hotelService.getReservedDatesForRoom(id).subscribe(result => this.reservedDates = result);
  }

  lock(){
    var lockStart = this.datepipe.transform(this.lockStart.value, 'yyyy-MM-dd');
    var lockEnd = this.datepipe.transform(this.lockEnd.value, 'yyyy-MM-dd');

    console.log(lockStart, lockEnd);

    this.http.post("/api/ReservedDate/reserve-room?roomId=" + this.id + "&startDate=" + lockStart + "&endDate=" + lockEnd, null).subscribe(()=>{
      window.location.reload();
    });
  }

  unlock(){
    var unlockStart = this.datepipe.transform(this.unlockStart.value, 'yyyy-MM-dd');
    var unlockEnd = this.datepipe.transform(this.unlockEnd.value, 'yyyy-MM-dd');

    console.log(unlockStart, unlockEnd);

    this.http.delete("/api/ReservedDate/delete-reserved-date?roomId=" + this.id +"&startDate=" + unlockStart + "&endDate=" + unlockEnd).subscribe(()=>{
      window.location.reload();
    });
  }

  editRoom(id: number) {
    this.router.navigate(['edit-room/' + id]);
  }

  inactivateRoom(id: number) {
    this.hotelService.inactivateRoom(id);
  }

  activateRoom(id: number) {
    this.hotelService.activateRoom(id);
  }

  //Getters
  get lockStart(){
    return this.roomEditForm.get('lockStart');
  }

  get lockEnd(){
    return this.roomEditForm.get('lockEnd');
  }

  get unlockStart(){
    return this.unlockForm.get('unlockStart');
  }

  get unlockEnd(){
    return this.unlockForm.get('unlockEnd');
  }
}
