import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-add-room',
  templateUrl: './add-room.component.html',
  styleUrls: ['./add-room.component.scss']
})
export class AddRoomComponent implements OnInit {

  id: number;
  roomAddForm: FormGroup;
  selectedFile: File = null;

  constructor(private fb: FormBuilder,
    private http: HttpClient,
    private route: ActivatedRoute,
    private router: Router) {
      this.id = parseInt(this.route.snapshot.paramMap.get("id"));

    }

  ngOnInit(): void {
    this.roomAddForm = this.fb.group({
      roomName: ['',
        [Validators.required]
      ],  
      type: ['',
        [Validators.required]
      ],
      maxPersons: ['',
        [Validators.required]
      ],
      pricePerPerson: ['',
        [Validators.required]
      ],
      image: ['',
        [Validators.required]
      ],
    });
    this.roomAddForm.valueChanges.subscribe(console.log); 
  }

  onFileSelected(event){
    this.selectedFile = <File>event.target.files[0];
  }

  submit(){
    const roomFormData = new FormData();
    roomFormData.append("RoomName", this.roomName.value);
    roomFormData.append("RoomType", this.type.value);
    roomFormData.append("MaxPersons", this.maxPersons.value);
    roomFormData.append("PricePerPerson", this.pricePerPerson.value);
    roomFormData.append("Image", this.selectedFile, this.selectedFile.name);
    roomFormData.append("AccomodationId", this.id.toString());

    console.log(roomFormData);

    this.http.post("api/Room", roomFormData).subscribe(result =>{
      this.router.navigate(["my-hotel/" + this.id]);
    }, error => {
      this.roomAddForm.reset();
      this.roomAddForm.setValidators(null);
    });
  }

  //Getters
  get roomName(){
    return this.roomAddForm.get('roomName');
  }

  get type(){
    return this.roomAddForm.get('type');
  }

  get maxPersons(){
    return this.roomAddForm.get('maxPersons');
  }

  get pricePerPerson(){
    return this.roomAddForm.get('pricePerPerson');
  }

}
