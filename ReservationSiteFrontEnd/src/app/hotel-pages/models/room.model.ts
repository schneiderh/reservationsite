import { Reservation } from "src/app/reservation/models/reservation.model";
import { Accomodation } from "./hotel.model";

export interface Room{
    "roomId": number;
    "roomName": string;
    "roomType": string;
    "maxPersons": number;
    "pricePerPerson": number;
    "imagePath": string;
    "isActive": boolean;
    "accomodationId": number;
    "accomodation": Accomodation,
    "reservations": Reservation[];
}