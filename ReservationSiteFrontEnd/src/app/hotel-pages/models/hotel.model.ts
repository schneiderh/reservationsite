import { AccomodationOwner } from "./accomodation-owner.model";

export interface Accomodation {
    "accomodationId": number;
    "accomodationName": string;
    "city": string;
    "type": string;
    "description": string;
    "offeredCatering": string;
    "prePayment": string;
    "cancellationFee": string;
    "accomodationOwnerId": number;
    "accomodationOwner": AccomodationOwner; 
    "isActive": boolean;
}