export interface AccomodationOwner {
    "accomodationOwnerId": number;
    "name": string;
    "email": string;
    "phoneNumber": string;
    "password": string;
    "salt": string;
}