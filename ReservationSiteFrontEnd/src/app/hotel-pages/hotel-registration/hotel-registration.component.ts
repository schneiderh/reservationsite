import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HotelSerivceService } from '../hotel-services/hotel-serivce.service';

@Component({
  selector: 'app-hotel-registration',
  templateUrl: './hotel-registration.component.html',
  styleUrls: ['./hotel-registration.component.scss']
})
export class HotelRegistrationComponent implements OnInit {

  constructor(private fb: FormBuilder, private http: HttpClient, private router: Router, private hotelService: HotelSerivceService) {}

  accomodationRegistrationForm: FormGroup;
  id = localStorage.getItem("id");
  success = true;

  ngOnInit(): void {
    this.accomodationRegistrationForm = this.fb.group({
      name: ['',
        [Validators.required]
      ],  
      city: ['',
        [Validators.required]
      ],
      type: ['',
        [Validators.required]
      ],
      description: ['',
        [Validators.required]
      ],
      catering: ['',
        [Validators.required]
      ],
      prepayment: ['',
        [Validators.required]
      ],
      cancellationFee: ['',
        [Validators.required]
      ],
    });
    this.accomodationRegistrationForm.valueChanges.subscribe(console.log); 
  }

  public submit(){
    
    const accomodationDto = {
        "AccomodationName": this.name.value,
        "City": this.hotelService.makeUpperCase(this.city.value),
        "Type": this.type.value,
        "Description": this.description.value,
        "OfferedCatering": this.catering.value,
        "PrePayment": parseFloat(this.prepayment.value),
        "CancellationFee": parseFloat(this.cancellationFee.value),
        "AccomodationOwnerId": parseInt(this.id)
      }

      this.http.post("/api/Accomodation", accomodationDto).subscribe(result =>{
        this.router.navigate(["hotel-registration"]);
      }, error => {
        this.accomodationRegistrationForm.reset();
        this.accomodationRegistrationForm.setValidators(null);
        this.success = false;
      });
  }

    //Getters
    get name(){
      return this.accomodationRegistrationForm.get('name');
    }
  
    get city(){
      return this.accomodationRegistrationForm.get('city');
    }
  
    get type(){
      return this.accomodationRegistrationForm.get('type');
    }

    get description(){
      return this.accomodationRegistrationForm.get('description');
    }
  
    get catering(){
      return this.accomodationRegistrationForm.get('catering');
    }
  
    get prepayment(){
      return this.accomodationRegistrationForm.get('prepayment');
    }

    get cancellationFee(){
      return this.accomodationRegistrationForm.get('cancellationFee');
    }
}
