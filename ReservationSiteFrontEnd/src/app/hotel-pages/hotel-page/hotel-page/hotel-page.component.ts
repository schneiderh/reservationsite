import { DatePipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Review } from 'src/app/user-pages/models/review.model';
import { UserServiceService } from 'src/app/user-pages/user-services/user-service.service';
import { HotelSerivceService } from '../../hotel-services/hotel-serivce.service';
import { Accomodation } from '../../models/hotel.model';
import { Room } from '../../models/room.model';

@Component({
  selector: 'app-hotel-page',
  templateUrl: './hotel-page.component.html',
  styleUrls: ['./hotel-page.component.scss']
})
export class HotelPageComponent implements OnInit {

  id: number;
  accomodation: Accomodation;
  activeRooms;
  reserveForm: FormGroup;

  minDate = new Date();
    year = this.minDate.getFullYear();
    month = this.minDate.getMonth();
    day = this.minDate.getDate();
  maxDate = new Date(this.year + 1, this.month, this.day);

  maxPersonDataArray: number[];
  maxPersons;

  reservedDates: string[];
  startFilter = (d: Date | null): boolean => {
    return !this.reservedDates.includes(this.datepipe.transform(d, 'yyyy-MM-dd'));
  }
  endFilter = (d: Date | null): boolean => {
    var tempDate: Date = new Date(d.getFullYear(), d.getMonth(), d.getDate() - 1);
    return this.startFilter(tempDate) && tempDate >= this.reservationStart.value;
  }

  reviews: Review[] = [];

  constructor(private router: Router,
    private route: ActivatedRoute,
    private hotelService: HotelSerivceService,
    private fb: FormBuilder,
    private datepipe: DatePipe,
    private http: HttpClient,
    private userService: UserServiceService
      ) {
    this.id = parseInt(this.route.snapshot.paramMap.get('id'));
   }

  ngOnInit(): void {
    this.getAccomodation(this.id).subscribe(result => this.accomodation = result);
    this.getActiveRoomsOfAccomodation(this.id).subscribe(result => this.activeRooms = result);
    this.getReviews();

    this.reserveForm = this.fb.group({
      choosenRoom: ['',
        [Validators.required]
      ],
      reservationStart: ['',
        [Validators.required]
      ],  
      reservationEnd: ['',
        [Validators.required]
      ],
      numberOfPersons: ['',
        [Validators.required]
      ],

    });
    this.reserveForm.valueChanges.subscribe(console.log);
  }

  getAccomodation(id:number) {
    return this.hotelService.getAccomodationById(id);
  }

  getActiveRoomsOfAccomodation(id: number){
    return this.hotelService.getActiveRoomsOfAccomodation(id);
  }

  getMaxPersonCountForRoom(){
    this.maxPersons = this.activeRooms[this.choosenRoom.value].maxPersons
    return this.activeRooms[this.choosenRoom.value].maxPersons;
  }

  dataForPersonsOptions(){
    var options: number[] = new Array(this.maxPersons);
    for (let index = 0; index < this.maxPersons; index++) {
      options[index] = index+1;
    }
    this.maxPersonDataArray = options;
  }

  getReservedDatesForRoom(){
    var roomId = this.activeRooms[this.choosenRoom.value].roomId;
    this.hotelService.getReservedDatesForRoom(roomId).subscribe(result => this.reservedDates = result);
  }

  getReviews(){
    this.hotelService.getRoomsOfAccomodation(parseInt(this.route.snapshot.paramMap.get('id'))).subscribe(rooms => {
      rooms.forEach(room => {
        this.hotelService.getAllReservationsOfRoom(room.roomId).subscribe(reservations => {
          reservations.forEach(reservation => {
            if(new Date(reservation.departure) < new Date()){
              this.userService.getReviewByReservationId(reservation.id).subscribe(review => {
                this.reviews.push(review);
                console.log(this.reviews);
              });
            }
          });
        });
      });
    });
  }

  reserve(){
    var roomId = this.activeRooms[this.choosenRoom.value].roomId;
    var id: number = parseInt(localStorage.getItem("id"));
    var startDate: string = this.datepipe.transform(this.reservationStart.value, 'yyyy-MM-dd');
    var endDate: string = this.datepipe.transform(this.reservationEnd.value, 'yyyy-MM-dd');
    var guestCount: number = this.numberOfPersons.value;

    console.log(roomId, id, startDate, endDate, guestCount);

    this.http.post('/api/Reservation/make-reservation?guestId=' + id + '&guestCount=' + guestCount + '&roomId=' + roomId + '&startDate=' + startDate + '&endDate=' + endDate, null).subscribe(
      () => this.router.navigate(['my-reservations/' + id])
    );
  }


  //Getters
  get choosenRoom(){
    return this.reserveForm.get('choosenRoom');
  }

  get reservationStart(){
    return this.reserveForm.get('reservationStart');
  }

  get reservationEnd(){
    return this.reserveForm.get('reservationEnd');
  }

  get numberOfPersons(){
    return this.reserveForm.get('numberOfPersons');
  }
}
