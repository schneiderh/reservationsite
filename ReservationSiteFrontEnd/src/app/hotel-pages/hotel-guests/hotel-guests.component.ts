import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserServiceService } from 'src/app/user-pages/user-services/user-service.service';
import { HotelSerivceService } from '../hotel-services/hotel-serivce.service';
import { Accomodation } from '../models/hotel.model';
import { Room } from '../models/room.model';

@Component({
  selector: 'app-hotel-guests',
  templateUrl: './hotel-guests.component.html',
  styleUrls: ['./hotel-guests.component.scss']
})
export class HotelGuestsComponent implements OnInit {

  accomodationId: number;
  rooms: Room[];
  accomodation: Accomodation;

  constructor(private route: ActivatedRoute,
    private hotelService: HotelSerivceService,
    private userService: UserServiceService,
    private router: Router) {
    this.accomodationId = parseInt(this.route.snapshot.paramMap.get('id'));
  }

  ngOnInit(): void {
    this.getData(this.accomodationId);
    this.getAccomodation(this.accomodationId);
  }

  getData(id: number){
    this.hotelService.getRoomsOfAccomodation(id).subscribe(rooms => {
      this.rooms = rooms;
      this.rooms.forEach(room => {
        this.hotelService.getAllReservationsOfRoom(room.roomId).subscribe(reservations => {
          room.reservations = reservations;
          
          room.reservations.forEach(reservation => {
            this.userService.getGuestById(reservation.guestId).subscribe(guest => {
              reservation.guest = guest;
            });
          });
        });
      });
    });
  }

  getAccomodation(id: number) {
    this.hotelService.getAccomodationById(id).subscribe(accomodation => this.accomodation = accomodation);
  }

  viewReservation(id: number): void {
    this.router.navigate(['reservation/' + id]);
  }
}
