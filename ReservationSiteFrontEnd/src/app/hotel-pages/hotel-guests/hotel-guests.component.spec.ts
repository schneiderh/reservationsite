import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HotelGuestsComponent } from './hotel-guests.component';

describe('HotelGuestsComponent', () => {
  let component: HotelGuestsComponent;
  let fixture: ComponentFixture<HotelGuestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HotelGuestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HotelGuestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
