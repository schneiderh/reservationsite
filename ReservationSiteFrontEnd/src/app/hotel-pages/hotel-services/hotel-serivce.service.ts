import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/internal/operators';
import { Reservation } from 'src/app/reservation/models/reservation.model';
import { Accomodation } from '../models/hotel.model'
import { Room } from '../models/room.model';

@Injectable({
  providedIn: 'root'
})
export class HotelSerivceService {

  constructor(private http: HttpClient, private router: Router) { }

  getMyAccomodations(id: number): Observable<Accomodation[]>{
    return this.http.get<Accomodation[]>('api/Accomodation/accomodations-of-owner?id=' + id);
  }

  getAccomodationById(id: number): Observable<Accomodation> {
    return this.http.get<Accomodation>("api/Accomodation/show?id=" + id);
  }

  inactivateAccomodation(id: number): void {
    this.http.put("/api/Accomodation/inactivate-accomodation?id=" + id, null).subscribe(()=>{window.location.reload()});
  }
  
  activateAccomodation(id: number): void {
    this.http.put("/api/Accomodation/activate-accomodation?id=" + id, null).subscribe(()=>{window.location.reload()});
  }

  deleteAccomodation(id: number): void {
    this.http.delete("/api/Accomodation/delete-accomodation?id=" + id).subscribe(() => {window.location.reload()});
  }

  getAccomodationsByCity(city: string): Observable<Accomodation[]> {
    return this.http.get<Accomodation[]>("/api/Accomodation/accomodations-of-city?city=" + city);
  }

  getRoomsOfAccomodation(id: number){
    return this.http.get<Room[]>("/api/Room/rooms-of-accomodation?id=" + id);
  }

  getRoomById(id:number){
    return this.http.get<Room>("/api/Room/show?id=" + id);
  }

  getReservedDatesForRoom(id:number){
    return this.http.get<string[]>("/api/ReservedDate/locked-days-for-room-from-now?roomId=" + id);
  }

  inactivateRoom(id: number): void {
    this.http.put("/api/Room/inactivate-room?id=" + id, null).subscribe(()=>{window.location.reload()});
  }
  
  activateRoom(id: number): void {
    this.http.put("/api/Room/activate-room?id=" + id, null).subscribe(()=>{window.location.reload()});
  }

  deleteRoom(id: number): void {
    this.http.delete("/api/Room/delete-room?id=" + id).subscribe(() => {window.location.reload()});
  }

  getActiveRoomsOfAccomodation(id: number): Observable<Room[]>{
    return this.http.get<Room[]>("/api/Room/active-rooms-of-accomodation?id=" + id);
  }

  getAllReservationsOfRoom(id: number): Observable<Reservation[]> {
    return this.http.get<Reservation[]>("/api/Reservation/get-reservations-of-room?roomId=" + id);
  }


  makeUpperCase(string: string): string {
    string = string[0].toUpperCase() + string.slice(1);
    return string;
  }
}
