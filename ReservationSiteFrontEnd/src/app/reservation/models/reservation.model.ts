import { Room } from "src/app/hotel-pages/models/room.model";
import { Guest } from "src/app/user-pages/models/user.model";

export interface Reservation {
    "id": number;
    "arrival": string;
    "departure": string;
    "isActive": boolean;
    "catering": string;
    "totalValue": string;
    "prePayment": number;
    "cancellationFee": number;
    "roomType": string;
    "guestId": number;
    "guest": Guest;
    "roomId": number;
    "room": Room;
    "reviewId": number;
    "reviewAble": boolean;
}