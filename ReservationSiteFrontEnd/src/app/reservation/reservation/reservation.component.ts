import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HotelSerivceService } from 'src/app/hotel-pages/hotel-services/hotel-serivce.service';
import { UserServiceService } from 'src/app/user-pages/user-services/user-service.service';
import { Reservation } from '../models/reservation.model';

@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.scss']
})
export class ReservationComponent implements OnInit {

  id: number;
  role: string = localStorage.getItem("role");
  reservation: Reservation;

  constructor(private userService: UserServiceService,
    private hotelService: HotelSerivceService,
    private route: ActivatedRoute,
    private router: Router,
    private http: HttpClient) {
    this.id = parseInt(this.route.snapshot.paramMap.get("id"));
  }

  ngOnInit(): void {
    this.getNeededData(this.id);
  }

  getNeededData(id: number){
    this.userService.getReservationById(id).subscribe(reservation => {
      this.reservation = reservation;
      this.hotelService.getRoomById(reservation.roomId).subscribe(room => {
        this.reservation.room = room;
        this.hotelService.getAccomodationById(room.accomodationId).subscribe(accomodation => {
          this.reservation.room.accomodation = accomodation;
          this.userService.getAccomodationOwner(accomodation.accomodationOwnerId).subscribe(accomodationOwner => {
            this.reservation.room.accomodation.accomodationOwner = accomodationOwner;
          });
        });
      });
      this.userService.getGuestById(reservation.guestId).subscribe(guest => {
        this.reservation.guest = guest;
        this.userService.getReviewByReservationId(reservation.id).subscribe(review => {
          if (review != null){
            reservation.reviewAble = new Date(reservation.departure) < new Date() ? true : false;
          } else {
            reservation.reviewAble = false;
          }
        });
      });
    });
  }

  viewAccomodation(id: number): void {
    this.router.navigate(['hotel/' + id]);
  }

  cancelReservation(id: number): void {
    this.http.put("/api/Reservation/cancel-reservation?id=" + id, null).subscribe(() => window.location.reload());
  }

  viewReview(id, accomodationName): void {
    this.router.navigate(['review-of-reservation/' + id + "/" + accomodationName]);
  }
}
