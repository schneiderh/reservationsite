import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HotelSerivceService } from 'src/app/hotel-pages/hotel-services/hotel-serivce.service';
import { Room } from 'src/app/hotel-pages/models/room.model';
import { UserServiceService } from 'src/app/user-pages/user-services/user-service.service';
import { Reservation } from '../../models/reservation.model';

@Component({
  selector: 'app-my-reservations',
  templateUrl: './my-reservations.component.html',
  styleUrls: ['./my-reservations.component.scss']
})
export class MyReservationsComponent implements OnInit {

  guestId: number;
  reservations: Reservation[]
  now: Date = new Date();
  rooms: Room[] = [];
  reservationDepature: Date;

  constructor(private userService: UserServiceService,
    private route: ActivatedRoute,
    private hotelService: HotelSerivceService,
    private router: Router) {
    this.guestId = parseInt(this.route.snapshot.paramMap.get('id'));
  }

  ngOnInit(): void {
    this.getReservationsOfUser(this.guestId);
  }

  getReservationsOfUser(id: number){
    this.userService.getReservationsOfUser(id).subscribe(reservations => {
      this.reservations = reservations;
      reservations.forEach(reservation => {
        this.userService.getReviewByReservationId(reservation.id).subscribe(review => {
          if (review != null){
            console.log(review);
            reservation.reviewAble = false;
          } else {
            reservation.reviewAble = new Date(reservation.departure) < new Date() ? true : false;
          }
        });
        this.hotelService.getRoomById(reservation.roomId).subscribe(room => {
          reservation.room = room;
          this.hotelService.getAccomodationById(room.accomodationId).subscribe(accomodation => room.accomodation = accomodation);
        });
      });
    });
  }

  viewAccomodation(id: number): void {
    this.router.navigate(['hotel/' + id]);
  }

  viewReservation(id: number): void {
    this.router.navigate(['reservation/' + id]);
  }

  leaveReview(id: number): void {
    this.router.navigate(['leave-review/' + id]);
  }

}
