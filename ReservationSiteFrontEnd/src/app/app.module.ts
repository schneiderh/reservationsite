import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponentComponent } from './login-component/login-component.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { FlexLayoutModule } from '@angular/flex-layout';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatToolbarModule } from '@angular/material/toolbar';
import { HomeComponent } from './home/home.component';
import { GuestRegistrationComponent } from './guest-registration/guest-registration.component';
import { OwnerRegistrationComponent } from './owner-registration/owner-registration.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MatIconModule } from '@angular/material/icon';
import { MatRadioModule } from '@angular/material/radio';
import { SearchComponent } from './search/search.component';
import { HotelRegistrationComponent } from './hotel-pages/hotel-registration/hotel-registration.component';
import { ForbiddenComponent } from './error-pages/forbidden/forbidden.component';
import { UserNotFoundComponent } from './error-pages/user-not-found/user-not-found.component';
import { UserWelcomeComponent } from './user-welcome/user-welcome.component';
import { AuthInterceptor } from './interceptors/auth-interceptor';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MyHotelsComponent } from './hotel-pages/my-hotels/my-hotels.component';
import { HotelEditComponent } from './hotel-pages/hotel-edit/hotel-edit/hotel-edit.component';
import { HotelPageComponent } from './hotel-pages/hotel-page/hotel-page/hotel-page.component';
import { ProfileComponent } from './user-pages/profile/profile/profile.component';
import { MyHotelComponent } from './hotel-pages/my-hotel/my-hotel/my-hotel.component';
import { RoomComponent } from './hotel-pages/room/room/room.component';
import { DatePipe } from '@angular/common';
import { EditRoomComponent } from './hotel-pages/edit-room/edit-room/edit-room.component';
import { AddRoomComponent } from './hotel-pages/add-room/add-room/add-room.component';
import { MatSelectModule } from '@angular/material/select';
import { ReservationComponent } from './reservation/reservation/reservation.component';
import { MyReservationsComponent } from './reservation/my-reservations/my-reservations/my-reservations.component';
import { HotelGuestsComponent } from './hotel-pages/hotel-guests/hotel-guests.component';
import { LeaveReviewComponent } from './user-pages/leave-review/leave-review.component';
import { ReviewComponent } from './user-pages/review/review.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponentComponent,
    GuestRegistrationComponent,
    OwnerRegistrationComponent,
    SearchComponent,
    HotelRegistrationComponent,
    ForbiddenComponent,
    UserNotFoundComponent,
    UserWelcomeComponent,
    MyHotelsComponent,
    HotelEditComponent,
    HotelPageComponent,
    ProfileComponent,
    MyHotelComponent,
    RoomComponent,
    EditRoomComponent,
    AddRoomComponent,
    ReservationComponent,
    MyReservationsComponent,
    HotelGuestsComponent,
    LeaveReviewComponent,
    ReviewComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatCardModule,
    MatToolbarModule,
    MatIconModule,
    MatRadioModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSidenavModule,
    MatListModule,
    MatSelectModule,
    HttpClientModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    DatePipe,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
