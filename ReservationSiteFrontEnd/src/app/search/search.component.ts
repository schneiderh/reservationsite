import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { HotelSerivceService } from '../hotel-pages/hotel-services/hotel-serivce.service';
import { Accomodation } from '../hotel-pages/models/hotel.model';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  searchForm: FormGroup;
  accomodations: Accomodation[];
  accomodationView = false;
  cityName;

  constructor(private fb: FormBuilder, private router: Router, private hotelService: HotelSerivceService, private route: ActivatedRoute) {
    try {
      this.cityName = this.hotelService.makeUpperCase(this.route.snapshot.paramMap.get("city"));
    } catch (error) {
      this.cityName = null;
    }
  }

  ngOnInit(): void {   
    if(typeof(this.cityName) === 'string') {
      this.accomodationView = true;
      this.submit(this.cityName);
    } 
    this.searchForm = this.fb.group({
      searchInput: ['',
        [Validators.required]
        ]
    });
    this.searchForm.valueChanges.subscribe(console.log);
  }

  submit(cityName = null) {
    if(cityName != null) {
      this.hotelService.getAccomodationsByCity(cityName).subscribe(result => this.accomodations = result);
      return;
    }
    const city = this.hotelService.makeUpperCase(this.searchInput.value);
    this.hotelService.getAccomodationsByCity(city).subscribe(result => {
      this.accomodations = result
      }
    );
    this.cityName = city;
    this.accomodationView = true;
  }

  redirectToHotelPage(id: number){
    window.open('hotel/' + id, '_blank');
  }

  get searchInput(){
    return this.searchForm.get('searchInput');
  }
}
