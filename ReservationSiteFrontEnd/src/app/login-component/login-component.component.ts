import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-component',
  templateUrl: './login-component.component.html',
  styleUrls: ['./login-component.component.scss']
})
export class LoginComponentComponent implements OnInit {

  constructor(private fb: FormBuilder, private http: HttpClient, private router: Router) {}

  loginForm: FormGroup;

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      email: ['',
        [Validators.required,
        Validators.email]
        ],  
      password: ['',
        [Validators.required]
      ],
      role: ['',
        [Validators.required]
      ]
    });
    this.loginForm.valueChanges.subscribe(console.log); 
  }

  hide = true;
  userNotFound = false;

  //A regisztrációs gomb mögötti tennivalók
  public submit(){
    
    const loginDto = {
        "Email": this.email.value,
        "Password": this.password.value,
        "Role": parseInt(this.role.value, 10)
      }

      this.http.post("/api/Authorization/login",loginDto, {responseType: "text"}).subscribe(async result =>{
        var jwtParts = result.toString().split(".");
        await localStorage.setItem("id_token", jwtParts[0] + "." + jwtParts[1] + "." + jwtParts[2]);
        await localStorage.setItem("role", jwtParts[3]);
        await localStorage.setItem("name", jwtParts[4]);
        await localStorage.setItem("id", jwtParts[5]);
        this.router.navigate(["welcome"]).then( success => window.location.reload());
      }, error => {
        this.loginForm.reset();
        this.loginForm.setValidators(null);
        this.userNotFound = true;
      });
  }

  //Getters
  get email(){
    return this.loginForm.get('email');
  }

  get password(){
    return this.loginForm.get('password');
  }

  get role(){
    return this.loginForm.get('role');
  }

}
