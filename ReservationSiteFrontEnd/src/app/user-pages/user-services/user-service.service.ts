import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AccomodationOwner } from 'src/app/hotel-pages/models/accomodation-owner.model';
import { Reservation } from 'src/app/reservation/models/reservation.model';
import { Review } from '../models/review.model';
import { Guest, Owner, User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {

  constructor(private http: HttpClient) { }

  getCurrentUser(role: string) {
    var id = localStorage.getItem("id");

    if(role === "Guest") {
      return this.http.get("api/Guests/show?id=" + id);
    }
    return this.http.get("api/AccomodationOwner/show?id=" + id);
  }

  getGuestById(id: number): Observable<Guest> {
    return this.http.get<Guest>("/api/Guests/show?id=" + id);
  }

  getAccomodationOwner(id: number): Observable<AccomodationOwner> {
    return this.http.get<AccomodationOwner>("/api/AccomodationOwner/show?id=" + id);
  }

  getReservationsOfUser(id: number): Observable<Reservation[]> {
    return this.http.get<Reservation[]>("/api/Reservation/reservations-of-user?id=" + id);
  }

  getReservationById(id: number): Observable<Reservation> {
    return this.http.get<Reservation>("/api/Reservation/show?id=" + id);
  }

  getReviewByReservationId(id: number): Observable<Review> {
    return this.http.get<Review>("/api/Review/review-of-reservation?reservationId=" + id);
  }

}
