import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { HotelSerivceService } from 'src/app/hotel-pages/hotel-services/hotel-serivce.service';
import { Accomodation } from 'src/app/hotel-pages/models/hotel.model';
import { Room } from 'src/app/hotel-pages/models/room.model';
import { Reservation } from 'src/app/reservation/models/reservation.model';
import { UserServiceService } from '../user-services/user-service.service';

@Component({
  selector: 'app-leave-review',
  templateUrl: './leave-review.component.html',
  styleUrls: ['./leave-review.component.scss']
})
export class LeaveReviewComponent implements OnInit {

  reservationId: number;
  accomodation: Accomodation;
  room: Room;
  reviewForm: FormGroup;
  reservation: Reservation;

  constructor(private fb: FormBuilder,
    private router: Router,
    private hotelService: HotelSerivceService,
    private route: ActivatedRoute,
    private userService: UserServiceService,
    private http: HttpClient) {
    this.reservationId = parseInt(this.route.snapshot.paramMap.get('reservationId'));
  }

  ngOnInit(): void {
    this.getData(this.reservationId);

    this.reviewForm = this.fb.group({
      score: ['',
        [Validators.required]
      ],  
      review: ['',
        [Validators.required]
      ],
    });
    this.reviewForm.valueChanges.subscribe(console.log);

    this.getReservation(this.reservationId);
  }

  getData(id: number) {
    this.userService.getReservationById(id).subscribe(reservation => {
      this.hotelService.getRoomById(reservation.roomId).subscribe(room => {
        this.hotelService.getAccomodationById(room.accomodationId).subscribe(accomodation => {
          this.reservation = reservation;
          this.accomodation = accomodation;
          this.room = room;
        });
      });
    });
  }

  getReservation(id: number){
    this.userService.getReservationById(this.reservationId).subscribe(result => this.reservation = result);
  }

  postReview(id: number) {
    const reviewDto = {
      "WrittenReview": this.review.value,
      "Score": parseInt(this.score.value),
      "ReservationId": this.reservationId
    }

    this.http.post("/api/Review", reviewDto).subscribe(() => this.router.navigate(['review-of-reservation/' + this.reservationId + "/" + this.accomodation.accomodationName]));
  }

  //Getters
  get score(){
    return this.reviewForm.get('score');
  }

  get review(){
    return this.reviewForm.get('review');
  }
}
