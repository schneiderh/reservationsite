import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from '../../models/user.model';
import { UserServiceService } from '../../user-services/user-service.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  role = localStorage.getItem("role");
  id = parseInt(localStorage.getItem("id"));
  user;
  guestDataEditForm: FormGroup;
  ownerDataEditForm: FormGroup;

  constructor(private userService: UserServiceService, private fb: FormBuilder, private http: HttpClient, private router: Router) { }

  ngOnInit(): void {
    this.getCurrentUser(this.role).subscribe(result => this.user = result);

    this.guestDataEditForm = this.fb.group({
      name: ['',
      ],  
      email: ['',
      ],
      phoneNumber: ['',
      ],
      zip: ['',
      ],
      city: ['',
      ],
      street: ['',
      ],
      houseNumber: ['',
      ],
    });
    this.guestDataEditForm.valueChanges.subscribe(console.log);

    this.ownerDataEditForm = this.fb.group({
      name: ['',
      ],  
      email: ['',
      ],
      phoneNumber: ['',
      ],
    });
    this.ownerDataEditForm.valueChanges.subscribe(console.log); 
  }

  getCurrentUser(role: string) {
    return this.userService.getCurrentUser(role);
  }

  submitGuestChange(){
    const guestDto = {
      "Name": (this.name.value === '') ? this.user.name : this.name.value,
      "Email": (this.email.value === '') ? this.user.email : this.email.value,
      "PhoneNumber": (this.phoneNumber.value === '') ? this.user.phoneNumber : this.phoneNumber.value,
      "Zip": (this.zip.value === '') ? this.user.zip : this.zip.value,
      "City": (this.city.value === '') ? this.user.city : this.city.value,
      "Street": (this.street.value === '') ? this.user.street : this.street.value,
      "HouseNumber": (this.houseNumber.value === '') ? this.user.houseNumber : this.houseNumber.value,
    }

    this.http.put("/api/Guests/edit-guest?id=" + this.id, guestDto).subscribe(result =>{
      window.location.reload();
    }, error => {
      this.guestDataEditForm.reset();
      this.guestDataEditForm.setValidators(null);
    });
  }

  submitOwnerChange(){
    const guestDto = {
      "Name": (this.name.value === '') ? this.user.name : this.name.value,
      "Email": (this.email.value === '') ? this.user.email : this.email.value,
      "PhoneNumber": (this.phoneNumber.value === '') ? this.user.phoneNumber : this.phoneNumber.value,
    }

    this.http.put("/api/AccomodationOwner/editOwner?id=" + this.id, guestDto).subscribe(result =>{
      window.location.reload();
    }, error => {
      this.ownerDataEditForm.reset();
      this.ownerDataEditForm.setValidators(null);
    });
  }

  //Getters for guest form
  get name(){
    if(this.role === 'Guest'){
      return this.guestDataEditForm.get('name');
    }
    return this.ownerDataEditForm.get('name');
  }

  get email(){
    if(this.role === 'Guest'){
      return this.guestDataEditForm.get('email');
    }
    return this.ownerDataEditForm.get('email');
  }

  get phoneNumber(){
    if(this.role === 'Guest'){
      return this.guestDataEditForm.get('phoneNumber');
    }
    return this.ownerDataEditForm.get('phoneNumber');
  }

  get zip(){
    return this.guestDataEditForm.get('zip');
  }

  get city(){
    return this.guestDataEditForm.get('city');
  }

  get street(){
    return this.guestDataEditForm.get('street');
  }

  get houseNumber(){
    return this.guestDataEditForm.get('houseNumber');
  }

}
