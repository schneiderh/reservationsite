import { Reservation } from "src/app/reservation/models/reservation.model";

export class User {
    "Name": string;
    "Email": string;
    "PhoneNumber": string;
    "Role": number;
}

export class Guest extends User {
    "guestId": number;
    "name": string;
    "email": string;
    "password": string;
    "phoneNumber": string;
    "zip": string;
    "city": string;
    "street": string;
    "houseNumber": string;
    "adress": string;
    "reservations": Reservation[];
}

export class Owner extends User {
    "accomodationOwnerId": number;
    "name": string;
    "email": string;
    "phoneNumber": string;
    "password": string;
}

