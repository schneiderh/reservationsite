export interface Review {
    "writtenReview": string;
    "score": number;
    "reservationId": number,
}