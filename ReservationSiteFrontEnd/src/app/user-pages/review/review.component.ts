import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Review } from '../models/review.model';
import { UserServiceService } from '../user-services/user-service.service';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.scss']
})
export class ReviewComponent implements OnInit {

  reservationId: number;
  review: Review;
  accomodationName: string;

  constructor(private route: ActivatedRoute, private userService: UserServiceService) {
    this.reservationId = parseInt(this.route.snapshot.paramMap.get("id"));
    this.accomodationName = this.route.snapshot.paramMap.get("accomodationName");
  }

  ngOnInit(): void {
    this.getReview(this.reservationId);
  }

  getReview(reservationId: number) {
    this.userService.getReviewByReservationId(reservationId).subscribe(review => {
      this.review = review;
    });
  }

}
