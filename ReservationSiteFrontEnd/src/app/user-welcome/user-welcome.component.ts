import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-welcome',
  templateUrl: './user-welcome.component.html',
  styleUrls: ['./user-welcome.component.scss']
})
export class UserWelcomeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  name = localStorage.getItem("name");
  role = localStorage.getItem("role");
  id = parseInt(localStorage.getItem("id"));

}
