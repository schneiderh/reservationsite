import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-guest-registration',
  templateUrl: './guest-registration.component.html',
  styleUrls: ['./guest-registration.component.scss']
})
export class GuestRegistrationComponent implements OnInit {

  constructor(private fb: FormBuilder, private http: HttpClient, private router: Router) {}

  guestRegistrationForm: FormGroup;

  ngOnInit(): void {
    this.guestRegistrationForm = this.fb.group({
      email: ['',
        [Validators.required,
        Validators.email]
        ],  
      name: ['',
        [Validators.required]
      ],
      phoneNumber: ['',
        [Validators.required]
      ],
      zip: ['',
        [Validators.required,
        Validators.minLength(4)]
      ],
      city: ['',
        [Validators.required]
      ],
      street: ['',
        [Validators.required]
      ],
      houseNumber: ['',
        [Validators.required]
      ],
      password: ['',
        [Validators.required,
          //kisbetű, nagybetű, szám és spec. karakter szükséges Minimum 4 karakter kell a jelszóba
        Validators.pattern("^((.*[a-z].*)[0-9](.*[a-z]?.*))|((.*[a-z]?.*)[0-9](.*[a-z].*))$"),
        Validators.minLength(4)]
      ]
    });
    this.guestRegistrationForm.valueChanges.subscribe(console.log); 
  }

  hide = true;

  //A regisztrációs gomb mögötti tennivalók
  public submit(){
    const guest = {
      "Email": this.email.value,
      "Name": this.name.value,
      "PhoneNumber": this.phoneNumber.value,
      "Zip": this.zip.value,
      "City": this.city.value,
      "Street": this.street.value,
      "HouseNumber": this.houseNumber.value,
      "Password": this.password.value
    }
    
    this.http.post("/api/Guests",guest).subscribe(()=>{
      this.router.navigate(["login"]);
    });
  }

  //Getters
  get email(){
    return this.guestRegistrationForm.get('email');
  }

  get password(){
    return this.guestRegistrationForm.get('password');
  }

  get name(){
    return this.guestRegistrationForm.get('name');
  }

  get zip(){
    return this.guestRegistrationForm.get('zip');
  }

  get city(){
    return this.guestRegistrationForm.get('city');
  }

  get street(){
    return this.guestRegistrationForm.get('street');
  }

  get houseNumber(){
    return this.guestRegistrationForm.get('houseNumber');
  }

  get phoneNumber(){
    return this.guestRegistrationForm.get('phoneNumber');
  }

}
