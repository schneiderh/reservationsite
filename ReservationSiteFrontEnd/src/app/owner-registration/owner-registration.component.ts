import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { LoginComponentComponent } from '../login-component/login-component.component';

@Component({
  selector: 'app-owner-registration',
  templateUrl: './owner-registration.component.html',
  styleUrls: ['./owner-registration.component.scss']
})
export class OwnerRegistrationComponent implements OnInit {

  constructor(private fb: FormBuilder, private http: HttpClient, private router: Router) {}

  ownerRegistrationForm: FormGroup;

  ngOnInit(): void {
    this.ownerRegistrationForm = this.fb.group({
      email: ['',
        [Validators.required,
        Validators.email]
        ],  
      name: ['',
        [Validators.required]
      ],
      phoneNumber: ['',
        [Validators.required]
      ],
      password: ['',
        [Validators.required,
          //kisbetű, nagybetű, szám és spec. karakter szükséges Minimum 4 karakter kell a jelszóba
        Validators.pattern("^((.*[a-z].*)[0-9](.*[a-z]?.*))|((.*[a-z]?.*)[0-9](.*[a-z].*))$"),
        Validators.minLength(4)]
      ]
    });
    this.ownerRegistrationForm.valueChanges.subscribe(console.log); 
  }

  hide = true;

  //A regisztrációs gomb mögötti tennivalók
  public submit(){
    const owner = {
      "Name": this.name.value,
      "Email": this.email.value,
      "PhoneNumber": this.phoneNumber.value,
      "Password": this.password.value
    }
    
    this.http.post("/api/AccomodationOwner",owner).subscribe(()=>{
      this.router.navigate(["login"]);
    });
  }

  //Getters
  get email(){
    return this.ownerRegistrationForm.get('email');
  }

  get password(){
    return this.ownerRegistrationForm.get('password');
  }

  get name(){
    return this.ownerRegistrationForm.get('name');
  }

  get phoneNumber(){
    return this.ownerRegistrationForm.get('phoneNumber');
  }
}
