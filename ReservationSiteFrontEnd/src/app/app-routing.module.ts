import { Component, NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ForbiddenComponent } from './error-pages/forbidden/forbidden.component';
import { UserNotFoundComponent } from './error-pages/user-not-found/user-not-found.component';
import { GuestRegistrationComponent } from './guest-registration/guest-registration.component';
import { HomeComponent } from './home/home.component';
import { AddRoomComponent } from './hotel-pages/add-room/add-room/add-room.component';
import { EditRoomComponent } from './hotel-pages/edit-room/edit-room/edit-room.component';
import { HotelEditComponent } from './hotel-pages/hotel-edit/hotel-edit/hotel-edit.component';
import { HotelGuestsComponent } from './hotel-pages/hotel-guests/hotel-guests.component';
import { HotelPageComponent } from './hotel-pages/hotel-page/hotel-page/hotel-page.component';
import { HotelRegistrationComponent } from './hotel-pages/hotel-registration/hotel-registration.component';
import { MyHotelComponent } from './hotel-pages/my-hotel/my-hotel/my-hotel.component';
import { MyHotelsComponent } from './hotel-pages/my-hotels/my-hotels.component';
import { RoomComponent } from './hotel-pages/room/room/room.component';
import { AuthGuardForGuest } from './interceptors/guest-auth.guard';
import { AuthGuardForOwner } from './interceptors/owner-auth.guard';
import { LoginComponentComponent } from './login-component/login-component.component';
import { OwnerRegistrationComponent } from './owner-registration/owner-registration.component';
import { MyReservationsComponent } from './reservation/my-reservations/my-reservations/my-reservations.component';
import { ReservationComponent } from './reservation/reservation/reservation.component';
import { SearchComponent } from './search/search.component';
import { LeaveReviewComponent } from './user-pages/leave-review/leave-review.component';
import { ProfileComponent } from './user-pages/profile/profile/profile.component';
import { ReviewComponent } from './user-pages/review/review.component';
import { UserWelcomeComponent } from './user-welcome/user-welcome.component';

const routes: Routes = [
  { path: 'login', component: LoginComponentComponent},
  { path: 'guest-registration', component: GuestRegistrationComponent},
  { path: 'owner-registration', component: OwnerRegistrationComponent},
  { path: 'search', component: SearchComponent, canActivate: [AuthGuardForGuest] },
  { path: 'search/:city', component: SearchComponent, canActivate: [AuthGuardForGuest] },
  { path: 'hotel-registration', component: HotelRegistrationComponent, canActivate: [AuthGuardForOwner] },
  { path: 'forbidden', component: ForbiddenComponent},
  { path: 'user-not-found', component: UserNotFoundComponent},
  { path: 'welcome', component: UserWelcomeComponent},
  { path: 'my-hotels', component: MyHotelsComponent, canActivate: [AuthGuardForOwner]},
  { path: 'edit-hotel', component: HotelEditComponent, canActivate: [AuthGuardForOwner]},
  { path: 'hotel/:id', component: HotelPageComponent, canActivate: [AuthGuardForGuest]},
  { path: 'my-profile', component: ProfileComponent},
  { path: 'my-hotel/:id', component: MyHotelComponent, canActivate: [AuthGuardForOwner]},
  { path: 'room/:id', component: RoomComponent, canActivate: [AuthGuardForOwner]},
  { path: 'edit-room/:id', component: EditRoomComponent, canActivate: [AuthGuardForOwner]},
  { path: 'hotel/:id/add-room', component: AddRoomComponent, canActivate: [AuthGuardForOwner]},
  { path: 'my-reservations/:id', component: MyReservationsComponent},
  { path: 'reservation/:id', component: ReservationComponent},
  { path: 'hotel-guests/:id', component: HotelGuestsComponent, canActivate: [AuthGuardForOwner]},
  { path: 'leave-review/:reservationId', component: LeaveReviewComponent, canActivate: [AuthGuardForGuest]},
  { path: 'review-of-reservation/:id/:accomodationName', component: ReviewComponent, canActivate: [AuthGuardForGuest]},
  
  
  { path:'', component: HomeComponent},
  { path: '**', redirectTo: '' }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
