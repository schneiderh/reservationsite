import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable({ providedIn: 'root' })
export class AuthGuardForOwner implements CanActivate {
    constructor(private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const currentUser = localStorage.getItem("id_token");
        const role = localStorage.getItem("role");

        if (!currentUser) {
            // not logged in so redirect to login page with the return url
            this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
            return false;
        }

        if(role === "Owner") {
            // logged in as owner so return true
            return true;
        }

        this.router.navigate(['forbidden'], { queryParams: { returnUrl: state.url } });
        return false;
    }
}