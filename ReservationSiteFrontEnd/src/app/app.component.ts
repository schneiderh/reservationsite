import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(private router: Router) {

  }
  title = 'ReservationSite-FrontEnd';
  role = localStorage.getItem("role");
  id = parseInt(localStorage.getItem("id"));

  logout() {
    localStorage.clear();
    this.role = null;
    this.router.navigate(["login"]);
  }
}
