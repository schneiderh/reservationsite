﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ReservationSite.Enums;
using ReservationSite.Models;
using ReservationSite.Models.Data_Transfer_Objects;
using ReservationSite.Services;

namespace ReservationSite.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccomodationOwnerController : ControllerBase
    {
        private readonly AccomodationOwnerService _accomodationOwnerService;

        public AccomodationOwnerController(AccomodationOwnerService accomodationOwnerService)
        {
            _accomodationOwnerService = accomodationOwnerService;
        }


        // GET: api/<AccomodationOwnerController>
        [HttpGet]
        public IEnumerable<AccomodationOwner> Get()
        {
            return _accomodationOwnerService.ListAccomodationOwners();
        }

        // GET api/<AccomodationOwnerController>/5
        //Itt nem kell, hogy ellenőrizzük, hogy szállásadó-e, mert vendég is kell, hogy lássa az adatait
        [HttpGet("show")]
        public async Task<AccomodationOwner> GetAccomodationOwnerById(int id)
        {
            AccomodationOwner accomodationOwner = await _accomodationOwnerService.getAccomodationOwnerById(id);
            if (accomodationOwner != null)
            {
                return accomodationOwner;
            }
            else
            {
                throw new Exception("No accomodation owner was found with given ID");
            }                
        }

        // POST api/<AccomodationOwnerController>
        //Itt nem kell, hogy ellenőrizzük, hiszen regisztrációról van szó
        [HttpPost]
        public async Task<int> PostAddAccomodationOwner([FromBody] AccomodationOwnerDto accomodationOwnerDto)
        {
            await _accomodationOwnerService.AddAccomodationOwner(accomodationOwnerDto);
            return StatusCodes.Status200OK;
        }

        // PUT api/<AccomodationOwnerController>/5
        //Itt kell, hogy ellenőrizzük, hiszen csak önmagát módosíthatja
        [HttpPut("editOwner")]
        public async Task<int> PutEditAccomodationOwnerData(int id, [FromBody] AccomodationOwnerDto ownerDto)
        {
            string header = HttpContext.Request.Headers["Authorization"];
            var decodedPayload = DecodeJwt(header);
            string role = (string)decodedPayload[ClaimTypes.Role];
            string nameIdentifier = (string)decodedPayload[ClaimTypes.NameIdentifier];
            if (role == Role.Owner.ToString() && nameIdentifier == id.ToString())
            {
                return await _accomodationOwnerService.EditAccomodationOwnerData(id, ownerDto);
            }
            return StatusCodes.Status401Unauthorized;
        }

        // DELETE api/<AccomodationOwnerController>/5
        //Előzőhöz hasonlóan, csak önmagát törölheti
        [HttpDelete("delete")]
        public async Task<int> Delete(int id)
        {
            string header = HttpContext.Request.Headers["Authorization"];
            var decodedPayload = DecodeJwt(header);
            string role = (string)decodedPayload[ClaimTypes.Role];
            string nameIdentifier = (string)decodedPayload[ClaimTypes.NameIdentifier];
            if (role == Role.Owner.ToString() && nameIdentifier == id.ToString())
            {
                return await _accomodationOwnerService.DeleteAccomodationOwner(id);
            }
            return StatusCodes.Status401Unauthorized;
        }

        public JwtPayload DecodeJwt(string header)
        {
            string[] jwtParts = header.Split(' ');
            string jwt = jwtParts[1];
            JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
            var jwtDecoded = handler.ReadJwtToken(jwt) as JwtSecurityToken;
            var payloadDecoded = jwtDecoded.Payload;
            return payloadDecoded;
        }
    }
}
