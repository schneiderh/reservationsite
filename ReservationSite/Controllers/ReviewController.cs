﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ReservationSite.Enums;
using ReservationSite.Models;
using ReservationSite.Models.Data_Transfer_Objects;
using ReservationSite.Services;

namespace ReservationSite.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReviewController : ControllerBase
    {
        private readonly ReviewService _reviewService;

        public ReviewController(ReviewService reviewService)
        {
            _reviewService = reviewService;
        }

        //Csak guest addolhat reviewt
        [HttpPost]
        public async Task<int> PostAddReview(ReviewDto reviewDto)
        {
            string header = HttpContext.Request.Headers["Authorization"];
            var decodedPayload = DecodeJwt(header);
            string role = (string)decodedPayload[ClaimTypes.Role];
            if (role == Role.Guest.ToString())
            {
                return await _reviewService.AddReview(reviewDto);
            }
            return StatusCodes.Status401Unauthorized;
        }

        //Csak a vendég nézheti meg a saját értékeléseit.
        [HttpGet("review-of-reservation")]
        public Review GetReviewOfReservation(int reservationId)
        {
            string header = HttpContext.Request.Headers["Authorization"];
            var decodedPayload = DecodeJwt(header);
            string role = (string)decodedPayload[ClaimTypes.Role];
            string id = (string)decodedPayload[ClaimTypes.NameIdentifier];
            int intGuestId = Int32.Parse(id);
            if (role == Role.Guest.ToString())
            {
                return _reviewService.ReviewsOfReservation(reservationId);
            }
            return null;
        }

        [HttpGet("show")]
        public Review GetReviewById(int reviewId)
        {
            string header = HttpContext.Request.Headers["Authorization"];
            var decodedPayload = DecodeJwt(header);
            string role = (string)decodedPayload[ClaimTypes.Role];
            string id = (string)decodedPayload[ClaimTypes.NameIdentifier];
            int intGuestId = Int32.Parse(id);
            if (role == Role.Guest.ToString())
            {
                return _reviewService.GetReviewById(reviewId);
            }
            return null;
        }

        public JwtPayload DecodeJwt(string header)
        {
            string[] jwtParts = header.Split(' ');
            string jwt = jwtParts[1];
            JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
            var jwtDecoded = handler.ReadJwtToken(jwt) as JwtSecurityToken;
            var payloadDecoded = jwtDecoded.Payload;
            return payloadDecoded;
        }
    }
}
