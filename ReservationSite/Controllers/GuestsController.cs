﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ReservationSite.DataAccessLayer;
using ReservationSite.Enums;
using ReservationSite.Models;
using ReservationSite.Models.Data_Transfer_Objects;
using ReservationSite.Services;

namespace ReservationSite.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GuestsController : ControllerBase
    {
        private readonly GuestService _guestService;
        private readonly HttpContextAccessor _httpContextAccessor;


        public GuestsController(GuestService guestService, HttpContextAccessor httpContextAccessor)
        {
            _guestService = guestService;
            _httpContextAccessor = httpContextAccessor;
        }

        //Itt nem szükséges role ellenőrzés, bárki regisztrálhat vendégnek
        [HttpPost]
        public async Task<int> PostAddGuest([FromBody]GuestDto guestDto)
        {
            return await _guestService.AddGuest(guestDto);
        }

        //Itt sem szükéges, mert mind a szállásadó és a vendég is megnézheti a vendég adatait
        [HttpGet("show")]
        public async Task<Guest> GetGuestById(int id)
        {
            return await _guestService.GetGuestById(id);
        }

        //Itt szükséges role ellenőrzés, hiszen csak a saját szállásának az adatait nézheti meg.
        [HttpGet("guests-of-accomodation")]
        public IEnumerable<Guest> GetAllGuestsOfAccomodation(int id)
        {
            string header = HttpContext.Request.Headers["Authorization"];
            var decodedPayload = DecodeJwt(header);
            string role = (string)decodedPayload[ClaimTypes.Role];
            if (role == Role.Owner.ToString())
            {
                return _guestService.GetAllGuestsOfAccomodation(id);
            }
            return null;
        }

        [HttpPut("edit-guest")]
        public async Task<int> PutEditGuestData(int id, [FromBody] GuestDto guestDto)
        {
            string header = HttpContext.Request.Headers["Authorization"];
            var decodedPayload = DecodeJwt(header);
            string role = (string)decodedPayload[ClaimTypes.Role];
            string nameIdentifier = (string)decodedPayload[ClaimTypes.NameIdentifier];
            if (role == Role.Guest.ToString() && nameIdentifier == id.ToString())
            {
                return await _guestService.EditGuestData(id, guestDto);
            }
            return StatusCodes.Status401Unauthorized;
        }

        public JwtPayload DecodeJwt(string header)
        {
            string[] jwtParts = header.Split(' ');
            string jwt = jwtParts[1];
            JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
            var jwtDecoded = handler.ReadJwtToken(jwt) as JwtSecurityToken;
            var payloadDecoded = jwtDecoded.Payload;
            return payloadDecoded;
        }

    }
}
