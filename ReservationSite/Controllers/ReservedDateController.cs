﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ReservationSite.Services;

namespace ReservationSite.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReservedDateController : ControllerBase
    {
        private readonly ReservedDateService _reservedDateService;

        public ReservedDateController(ReservedDateService reservedDateService)
        {
            _reservedDateService = reservedDateService;
        }

        //Ezeket a userek nem használják, a service-ben lévő metódusok kerülnek felhasználásra, a controller gyakorlatilag tesztelési szolgálatot lát el

        [HttpGet("is-free")]
        public bool GetCheckDateForRoom(DateTime startDate, DateTime endDate, int roomId)
        {
            return _reservedDateService.IsDateFree(startDate, endDate, roomId);
        }

        [HttpGet("locked-days-for-room-from-now")]
        public IEnumerable<string> GetReservedDatesForRoomFromNow(int roomId)
        {
            return _reservedDateService.GetReservedDatesForRoomFromNow(roomId);
        }

        [HttpPost("reserve-room")]
        public async Task<int> PostReserveRoom(int roomId, DateTime startDate, DateTime endDate)
        {
            return await _reservedDateService.ReserveRoomForInterval(roomId, startDate, endDate);
        }

        [HttpDelete("delete-reserved-date")]
        public async Task<int> DeleteReservedDate(int roomId, DateTime startDate, DateTime endDate)
        {
            return await _reservedDateService.DeleteReservedDate(roomId, startDate, endDate);
        }
    }
}
