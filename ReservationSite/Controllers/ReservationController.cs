﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ReservationSite.Enums;
using ReservationSite.Models;
using ReservationSite.Services;

namespace ReservationSite.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReservationController : ControllerBase
    {
        private readonly ReservationService _reservationService;

        public ReservationController(ReservationService reservationService)
        {
            _reservationService = reservationService;
        }

        //Csak guest teheti
        [HttpPost("make-reservation")]
        public async Task<int> PostMakeReservation(int guestCount, int roomId, DateTime startDate, DateTime endDate)
        {
            string header = HttpContext.Request.Headers["Authorization"];
            var decodedPayload = DecodeJwt(header);
            string role = (string)decodedPayload[ClaimTypes.Role];
            if (role == Role.Guest.ToString())
            {
                string guestId = (string)decodedPayload[ClaimTypes.NameIdentifier];
                int intGuestId = Int32.Parse(guestId);
                return await _reservationService.MakeReservation(intGuestId, guestCount, roomId, startDate, endDate);
            }
            return StatusCodes.Status401Unauthorized;
        }

        //Csak guest teheti, és csak a sajátjait
        [HttpGet("reservations-of-user")]
        public IEnumerable<Reservation> GetReservationsOfUser(int id)
        {
            string header = HttpContext.Request.Headers["Authorization"];
            var decodedPayload = DecodeJwt(header);
            string role = (string)decodedPayload[ClaimTypes.Role];
            string guestId = (string)decodedPayload[ClaimTypes.NameIdentifier];
            int intGuestId = Int32.Parse(guestId);
            if (role == Role.Guest.ToString() && intGuestId == id)
            {
                return _reservationService.GetReservationsOfGuest(id);
            }
            return null;
        }

        //Mind a szállásadó és a vendég is teheti
        [HttpGet("show")]
        public async Task<Reservation> GetReservationById(int id)
        {
            return await _reservationService.GetReservationById(id);
        }

        //Csak a guest teheti
        [HttpPut("cancel-reservation")]
        public async Task<int> PutCancelReservation(int id)
        {
            string header = HttpContext.Request.Headers["Authorization"];
            var decodedPayload = DecodeJwt(header);
            string role = (string)decodedPayload[ClaimTypes.Role];
            if (role == Role.Guest.ToString())
            {
                return await _reservationService.CancelReservation(id);
            }
            return StatusCodes.Status401Unauthorized;
        }

        [HttpPut("edit-reservation-date")]
        public async Task<int> PutModifyReservationDates(int id, DateTime newStartDate, DateTime newEndDate)
        {

            return await _reservationService.EditReservationDates(id, newStartDate, newEndDate);
        }

        //Mivel kell az értékelések megjelenítéséhez ezért nézheti szállás és vendég is
        [HttpGet("get-reservations-of-room")]
        public IEnumerable<Reservation> GetAllReservationsOfRoom(int roomId)
        {
            return _reservationService.GetAllReservationsOfRoom(roomId);
        }

        public JwtPayload DecodeJwt(string header)
        {
            string[] jwtParts = header.Split(' ');
            string jwt = jwtParts[1];
            JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
            var jwtDecoded = handler.ReadJwtToken(jwt) as JwtSecurityToken;
            var payloadDecoded = jwtDecoded.Payload;
            return payloadDecoded;
        }
    }
}
