﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ReservationSite.Enums;
using ReservationSite.Models;
using ReservationSite.Models.Data_Transfer_Objects;
using ReservationSite.Services;

namespace ReservationSite.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccomodationController : ControllerBase
    {
        private readonly AccomodationService _accomodationService;

        public AccomodationController(AccomodationService accomodationService)
        {
            _accomodationService = accomodationService;
        }

        //Bárki láthatja
        [HttpGet("show")]
        public async Task<Accomodation> GetAccomodationById(int id)
        {
            Accomodation accomodation = await _accomodationService.GetAccomodationById(id);
            if (accomodation != null)
            {
                return accomodation;
            }
            else
            {
                throw new Exception("No accomodation was found with given ID");
            }
        }

        //Bárki láthatja
        [HttpGet("accomodations-of-city")]
        public IEnumerable<Accomodation> GetAccomodationsByCity(string city)
        {
            return _accomodationService.GetAccomodationsByCity(city);
        }

        //Csak a szállásadó láthatja
        [HttpGet("accomodations-of-owner")]
        public IEnumerable<Accomodation> GetAccomodationsByOwnerId(int id)
        {
            string header = HttpContext.Request.Headers["Authorization"];
            var decodedPayload = DecodeJwt(header);
            string role = (string)decodedPayload[ClaimTypes.Role];
            if (role == Role.Owner.ToString())
            {
                return _accomodationService.GetAccomodationsOfOwner(id);
            }
            return null;
        }

        //Csak szállásadó teheti
        [HttpPost]
        public async Task<int> PostAddAccomodation(AccomodationDto accomodationDto)
        {
            string header = HttpContext.Request.Headers["Authorization"];
            var decodedPayload = DecodeJwt(header);
            string role = (string)decodedPayload[ClaimTypes.Role];
            if (role == Role.Owner.ToString())
            {
                return await _accomodationService.AddAccomodation(accomodationDto);
            }
            return StatusCodes.Status401Unauthorized;
        }
        //Csak szállásadó
        [HttpPut("edit")]
        public async Task<int> PutEditAccomodation(int id, AccomodationDto accomodationDto)
        {
            string header = HttpContext.Request.Headers["Authorization"];
            var decodedPayload = DecodeJwt(header);
            string role = (string)decodedPayload[ClaimTypes.Role];
            if (role == Role.Owner.ToString())
            {
                return await _accomodationService.EditAccomodationData(id, accomodationDto);
            }
            return StatusCodes.Status401Unauthorized;
        }

        //Csak szállásadó
        [HttpPut("inactivate-accomodation")]
        public async Task<int> PutInactivateAccomodation(int id)
        {
            string header = HttpContext.Request.Headers["Authorization"];
            var decodedPayload = DecodeJwt(header);
            string role = (string)decodedPayload[ClaimTypes.Role];
            if (role == Role.Owner.ToString())
            {
                return await _accomodationService.InactivateAccomodation(id);
            }
            return StatusCodes.Status401Unauthorized;
        }

        //Csak szállásadó
        [HttpPut("activate-accomodation")]
        public async Task<int> PutActivateAccomodation(int id)
        {
            string header = HttpContext.Request.Headers["Authorization"];
            var decodedPayload = DecodeJwt(header);
            string role = (string)decodedPayload[ClaimTypes.Role];
            if (role == Role.Owner.ToString())
            {
                return await _accomodationService.ActivateAccomodation(id);
            }
            return StatusCodes.Status401Unauthorized;
        }

        //Csak szállásadó
        [HttpDelete("delete-accomodation")]
        public async Task<int> DeleteAccomodation(int id)
        {
            string header = HttpContext.Request.Headers["Authorization"];
            var decodedPayload = DecodeJwt(header);
            string role = (string)decodedPayload[ClaimTypes.Role];
            if (role == Role.Owner.ToString())
            {
                return await _accomodationService.DeleteAccomodation(id);
            }
            return StatusCodes.Status401Unauthorized;
        }

        public JwtPayload DecodeJwt(string header)
        {
            string[] jwtParts = header.Split(' ');
            string jwt = jwtParts[1];
            JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
            var jwtDecoded = handler.ReadJwtToken(jwt) as JwtSecurityToken;
            var payloadDecoded = jwtDecoded.Payload;
            return payloadDecoded;
        }
    }
}
