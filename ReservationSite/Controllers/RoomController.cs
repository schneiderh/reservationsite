﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ReservationSite.Enums;
using ReservationSite.Models;
using ReservationSite.Models.Data_Transfer_Objects;
using ReservationSite.Services;

namespace ReservationSite.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoomController : ControllerBase
    {
        private readonly RoomService _roomService;

        public RoomController(RoomService roomService)
        {
            _roomService = roomService;
        }

        //Csak szállásadó adhat hozzá
        [HttpPost, DisableRequestSizeLimit]
        public async Task<int> PostAddRoom([FromForm]RoomDto roomDto)
        {
            string header = HttpContext.Request.Headers["Authorization"];
            var decodedPayload = DecodeJwt(header);
            string role = (string)decodedPayload[ClaimTypes.Role];
            if (role == Role.Owner.ToString())
            {
                await _roomService.AddRoom(roomDto);
                return StatusCodes.Status200OK;
            }
            return StatusCodes.Status401Unauthorized;
                
        }

        //Mindenki megnézheti
        [HttpGet("active-rooms-of-accomodation")]
        public IEnumerable<Room> GetAllActiveRoomsOfAccomodation(int id)
        {
            return _roomService.GetAllActiveRoomsOfAccomodation(id);
        }

        //Szükséges az értékelések megjelenítéséhez, ezért vendég és szállás is nézheti
        [HttpGet("rooms-of-accomodation")]
        public IEnumerable<Room> GetAllRoomsOfAccomodation(int id)
        {
            return _roomService.GetAllRoomsOfAccomodation(id);
        }

        //Mindenki megnézheti
        [HttpGet("show")]
        public async Task<Room> GetRoomById(int id)
        {
            Room room = await _roomService.GetRoomById(id);
            if(room != null)
            {
                return room;
            }
            throw new Exception("No room found with given ID");
        }

        //Csak szállásadó
        [HttpPut("edit")]
        public async Task<int> PutEditRoom(int id, [FromForm]RoomDto roomDto, bool withImage)
        {
            string header = HttpContext.Request.Headers["Authorization"];
            var decodedPayload = DecodeJwt(header);
            string role = (string)decodedPayload[ClaimTypes.Role];
            if (role == Role.Owner.ToString())
            {
                await _roomService.EditRoomDetails(id, roomDto, withImage);
                return StatusCodes.Status200OK;
            }
            return StatusCodes.Status401Unauthorized;
        }

        //Csak szállásadó
        [HttpPut("inactivate-room")]
        public async Task<int> PutInactivateRoom(int id)
        {
            string header = HttpContext.Request.Headers["Authorization"];
            var decodedPayload = DecodeJwt(header);
            string role = (string)decodedPayload[ClaimTypes.Role];
            if (role == Role.Owner.ToString())
            {
                return await _roomService.InactivateRoom(id);
            }
            return StatusCodes.Status401Unauthorized;
        }

        [HttpPut("activate-room")]
        public async Task<int> PutActivateRoom(int id)
        {
            string header = HttpContext.Request.Headers["Authorization"];
            var decodedPayload = DecodeJwt(header);
            string role = (string)decodedPayload[ClaimTypes.Role];
            if (role == Role.Owner.ToString())
            {
                return await _roomService.ActivateRoom(id);
            }
            return StatusCodes.Status401Unauthorized;
        }

        //Csak szállásadó
        [HttpDelete("delete-room")]
        public async Task<int> DeleteRoom(int id)
        {
            string header = HttpContext.Request.Headers["Authorization"];
            var decodedPayload = DecodeJwt(header);
            string role = (string)decodedPayload[ClaimTypes.Role];
            if (role == Role.Owner.ToString())
            {
                return await _roomService.DeleteRoom(id);
            }
            return StatusCodes.Status401Unauthorized;
        }

        public JwtPayload DecodeJwt(string header)
        {
            string[] jwtParts = header.Split(' ');
            string jwt = jwtParts[1];
            JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
            var jwtDecoded = handler.ReadJwtToken(jwt) as JwtSecurityToken;
            var payloadDecoded = jwtDecoded.Payload;
            return payloadDecoded;
        }
    }
}
