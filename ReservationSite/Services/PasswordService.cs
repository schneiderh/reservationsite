﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using ReservationSite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web.Helpers;

namespace ReservationSite.Services
{
    public class PasswordService
    {
        public PasswordService()
        {

        }

        public Password HashPassword(string password, byte[] salt = null)
        {
            if(salt == null)
            {
                salt = new byte[128 / 8];
                using (var rng = RandomNumberGenerator.Create())
                {
                    rng.GetBytes(salt);
                }
            }

            string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: password,
                salt: salt,
                prf: KeyDerivationPrf.HMACSHA256,
                iterationCount: 10000,
                numBytesRequested: 256 / 8));

            Password hashSalt = new Password(hashed, Convert.ToBase64String(salt));
            return hashSalt;
        }

        public bool VerifyPassword(string enteredPassword, string storedHash, string storedSalt)
        {
            var salt = Convert.FromBase64String(storedSalt);
            
            var hashOfpasswordToCheck = this.HashPassword(enteredPassword, salt).PasswordHash;

            if (storedHash == hashOfpasswordToCheck)
            {
                return true;
            }
            return false;

        }

    }
}
