﻿using Microsoft.AspNetCore.Http;
using ReservationSite.DataAccessLayer;
using ReservationSite.Models;
using ReservationSite.Models.Data_Transfer_Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReservationSite.Services
{
    public class AccomodationOwnerService
    {
        private readonly ReservationSiteContext _reservationSiteContext;
        private readonly PasswordService _passwordService;


        public AccomodationOwnerService(ReservationSiteContext reservationSiteContext, PasswordService passwordService)
        {
            _reservationSiteContext = reservationSiteContext;
            _passwordService = passwordService;
        }

        public IEnumerable<AccomodationOwner> ListAccomodationOwners()
        {
            var accomodationOwners = _reservationSiteContext.AccomodationOwners;
            return accomodationOwners.ToList();
        }

        public async Task<IEnumerable<AccomodationOwner>> AddAccomodationOwner(AccomodationOwnerDto accomodationOwnerDto)
        {
            AccomodationOwner accomodationOwner = new AccomodationOwner(accomodationOwnerDto.Name, accomodationOwnerDto.Email, accomodationOwnerDto.PhoneNumber, accomodationOwnerDto.Password);
            await _reservationSiteContext.AccomodationOwners.AddAsync(accomodationOwner);
            this.SetPassword(accomodationOwner, accomodationOwnerDto.Password);
            _reservationSiteContext.SaveChanges();
            var accomodationOwners = _reservationSiteContext.AccomodationOwners;
            return accomodationOwners.ToList();
        }

        public async Task<AccomodationOwner> getAccomodationOwnerById(int id)
        {
            AccomodationOwner accomodationOwner = await _reservationSiteContext.AccomodationOwners.FindAsync(id);
            if(accomodationOwner != null)
            {
                return accomodationOwner;
            }
            return null;
        }

        public async Task<int> EditAccomodationOwnerData(int id, AccomodationOwnerDto ownerDto)
        {
            AccomodationOwner ownerToEdit = await _reservationSiteContext.AccomodationOwners.FindAsync(id);
            if (ownerToEdit != null)
            {
                ownerToEdit.Email = ownerDto.Email;
                ownerToEdit.Name = ownerDto.Name;
                ownerToEdit.PhoneNumber = ownerDto.PhoneNumber;
                _reservationSiteContext.SaveChanges();
                return StatusCodes.Status200OK;
            }
            throw new Exception("No accomodation owner was found with given ID");

        }

        public async Task<int> DeleteAccomodationOwner(int id)
        {
            var ownerToDelete = _reservationSiteContext.AccomodationOwners.Find(id);
            if (ownerToDelete != null)
            {
                _reservationSiteContext.AccomodationOwners.Remove(ownerToDelete);
                _reservationSiteContext.SaveChanges();
                return StatusCodes.Status200OK;
            }
            throw new Exception("No accomodation owner was found with given ID");
        }

        public async Task<AccomodationOwner> GetOwnerByEmail(string email)
        {
            AccomodationOwner accomodationOwner = _reservationSiteContext.AccomodationOwners.Where(accomodationOwner 
                => accomodationOwner.Email == email).SingleOrDefault();
            return accomodationOwner;
        }

        public void SetPassword(AccomodationOwner accomodationOwner, string password)
        {
            Password pswd = _passwordService.HashPassword(password);
            accomodationOwner.Password = pswd.PasswordHash;
            accomodationOwner.Salt = pswd.Salt;

        }
    }
}
