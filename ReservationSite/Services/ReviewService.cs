﻿using Microsoft.AspNetCore.Http;
using ReservationSite.DataAccessLayer;
using ReservationSite.Models;
using ReservationSite.Models.Data_Transfer_Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReservationSite.Services
{
    public class ReviewService
    {
        private readonly ReservationSiteContext _reservationSiteContext;
        private readonly AccomodationService _accomodationService;
        private readonly RoomService _roomService;
        private readonly ReservationService _reservationService;

        public ReviewService(ReservationSiteContext reservationSiteContext, AccomodationService accomodationService, RoomService roomService, ReservationService reservationService)
        {
            _reservationSiteContext = reservationSiteContext;
            _accomodationService = accomodationService;
            _roomService = roomService;
            _reservationService = reservationService;
        }

        public async Task<int> AddReview(ReviewDto reviewDto)
        {
            Reservation reservation = _reservationService.GetReservationById(reviewDto.ReservationId).Result;
            if(reservation.Departure < DateTime.Today)
            {
                Review review = new Review(reviewDto.WrittenReview, reviewDto.Score, reviewDto.ReservationId);
                await _reservationSiteContext.Reviews.AddAsync(review);
                await _reservationSiteContext.SaveChangesAsync();
                return StatusCodes.Status200OK;
            }
            return StatusCodes.Status400BadRequest;
        }

        public Review ReviewsOfReservation(int reservationId)
        {
            try
            {
                Review review = _reservationSiteContext.Reviews.Where(review => review.ReservationId == reservationId).FirstOrDefault();
                return review;
            }
            catch (Exception e)
            {
                throw new Exception("Review not found" + e.Message);
            }
        }

        public Review GetReviewById(int id)
        {
            try
            {
                Review review = _reservationSiteContext.Reviews.Find(id);
                return review;
            }
            catch (Exception e)
            {
                throw new Exception("Review not found" + e.Message);
            }
        }

        public async Task<int> EditReview(int reviewId, ReviewDto reviewDto)
        {
            Review review = _reservationSiteContext.Reviews.Find(reviewId);
            if (review != null)
            {
                review.WrittenReview = reviewDto.WrittenReview;
                review.Score = reviewDto.Score;
                await _reservationSiteContext.SaveChangesAsync();
                return StatusCodes.Status200OK;
            }
            throw new Exception("Review modification was unsuccesful");
        }
    }
}
