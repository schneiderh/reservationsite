﻿using Microsoft.AspNetCore.Http;
using ReservationSite.DataAccessLayer;
using ReservationSite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReservationSite.Services
{
    public class ReservedDateService
    {
        private readonly ReservationSiteContext _reservationSiteContext;
        public ReservedDateService(ReservationSiteContext reservationSiteContext)
        {
            _reservationSiteContext = reservationSiteContext;
        }

        public bool IsDateFree(DateTime startDate, DateTime endDate, int roomId)
        {
            int stayLength = this.GetStayLengthInInt(startDate, endDate);

            if(startDate.Year < DateTime.Now.Year
                || (startDate.Year == DateTime.Now.Year && startDate.Month < DateTime.Now.Month)
                || (startDate.Year == DateTime.Now.Year && startDate.Month == DateTime.Now.Month && startDate.Day < DateTime.Now.Day))
            {
                return false;
            }

            for(int i = 0; i < stayLength; i++)
            {
                DateTime checkDate = startDate.AddDays(i);
                if (_reservationSiteContext.ReservedDates.Where(date => date.RoomId == roomId && date.Date == checkDate).Count() != 0)
                {
                    return false;
                }
            }
            return true;
        }

        public int GetStayLengthInInt(DateTime startDate, DateTime endDate)
        {
            TimeSpan lenght = endDate - startDate;
            return (int)lenght.TotalDays;
        }

        public IEnumerable<string> GetReservedDatesForRoomFromNow(int roomId)
        {
            DateTime now = DateTime.Today;
            IEnumerable<ReservedDate> reservedDates = _reservationSiteContext.ReservedDates.Where(date => date.RoomId == roomId && date.Date >= now);

            List<string> dates = new List<string>();
            foreach (var reservedDate in reservedDates)
            {
                dates.Add(reservedDate.Date.ToString("yyyy-MM-dd"));
            }

            return dates;
        }

        private int GetDayForIntervalCheck(int year, int month)
        {
            //Ha nem szökőév februárját választja
            if (month == 2 && year % 4 != 0)
            {
                return 28;
            }
            //Ha szőkőév februárját választja
            else if (month == 2 && year % 4 == 0)
            {
                return 29;
            }
            //Ha 31 napos hónapot választ
            else if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10)
            {
                return 31;
            }
            //Ha 30 napos hónapot választ
            else
            {
                return 30;
            }
        }

        public async Task<int> ReserveRoomForInterval(int roomId, DateTime startDate, DateTime endDate)
        {
            if(startDate.Year < DateTime.Now.Year
                || (startDate.Year == DateTime.Now.Year && startDate.Month < DateTime.Now.Month)
                || (startDate.Year == DateTime.Now.Year && startDate.Month == DateTime.Now.Month && startDate.Day < DateTime.Now.Day))
            {
                throw new Exception("Unable to reserve date, because date is in the past");
            }
            int length = this.GetStayLengthInInt(startDate, endDate);
            try
            {
                for (int i = 0; i < length; i++)
                {
                    DateTime dateToReserve = startDate.AddDays(i);
                    ReservedDate date = new ReservedDate(dateToReserve, roomId);
                    await _reservationSiteContext.ReservedDates.AddAsync(date);
                }
                _reservationSiteContext.SaveChanges();
                return StatusCodes.Status200OK;
            }
            catch (Exception)
            {
                throw new Exception("There was an error during the reservation process. Please try again later, or contact our cutomer service");
            }
        }

        public async Task<int> DeleteReservedDate(int roomId, DateTime startDate, DateTime endDate)
        {
            if(startDate.Year < DateTime.Now.Year
                || (startDate.Year == DateTime.Now.Year && startDate.Month < DateTime.Now.Month)
                || (startDate.Year == DateTime.Now.Year && startDate.Month == DateTime.Now.Month && startDate.Day < DateTime.Now.Day))
            {
                throw new Exception("A past reservation can not be cancelled");
            }
            int stayLength = this.GetStayLengthInInt(startDate, endDate);
            for(int i = 0; i < stayLength; i++)
            {
                DateTime date = startDate.AddDays(i);
                ReservedDate reservedDate = _reservationSiteContext.ReservedDates.Where(entry => entry.RoomId == roomId && entry.Date == date).First();
                _reservationSiteContext.ReservedDates.Remove(reservedDate);
            }
            _reservationSiteContext.SaveChanges();
            return StatusCodes.Status200OK;
        }
    }
}
