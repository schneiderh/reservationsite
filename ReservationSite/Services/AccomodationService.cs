﻿using Microsoft.AspNetCore.Http;
using ReservationSite.DataAccessLayer;
using ReservationSite.Models;
using ReservationSite.Models.Data_Transfer_Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;

namespace ReservationSite.Services
{
    public class AccomodationService
    {
        private readonly ReservationSiteContext _reservationSiteContext;
        private readonly AccomodationOwnerService _accomodationOwnerService;


        public AccomodationService(ReservationSiteContext reservationSiteContext, AccomodationOwnerService accomodationOwnerService)
        {
            _reservationSiteContext = reservationSiteContext;
            _accomodationOwnerService = accomodationOwnerService;
        }

        public async Task<Accomodation> GetAccomodationById(int id)
        {
            Accomodation accomodation = await _reservationSiteContext.Accomodations.FindAsync(id);
            //_reservationSiteContext.Entry(accomodation).Reference("AccomodationOwner").Load(); <--- azoknak a betöltése, ami még szerepel benne

            if (accomodation != null)
            {
                return accomodation;
            }
            return null;
        }

        public IEnumerable<Accomodation> GetAccomodationsByCity(string city)
        {
            IEnumerable<Accomodation> accomodations = _reservationSiteContext.Accomodations.Where(accomodation => accomodation.City == city);
            return accomodations;
        }

        public IEnumerable<Accomodation> GetAccomodationsOfOwner(int id)
        {
            IEnumerable<Accomodation> accomodations = _reservationSiteContext.Accomodations.Where(accomodation => accomodation.AccomodationOwnerId == id);
            return accomodations;
        }

        public async Task<int> AddAccomodation(AccomodationDto accomodationDto)
        {
            Accomodation accomodation = new Accomodation
                (
                    accomodationDto.AccomodationName,
                    accomodationDto.City,
                    accomodationDto.Type,
                    accomodationDto.Description,
                    accomodationDto.OfferedCatering,
                    accomodationDto.PrePayment,
                    accomodationDto.CancellationFee,
                    accomodationDto.AccomodationOwnerId
                );
            await _reservationSiteContext.Accomodations.AddAsync(accomodation);
            await _reservationSiteContext.SaveChangesAsync();
            return StatusCodes.Status200OK;
        }

        public async Task<int> EditAccomodationData(int id, AccomodationDto accomodationDto)
        {
            Accomodation accomodation = await _reservationSiteContext.Accomodations.FindAsync(id);
            if (accomodation != null)
            {
                accomodation.AccomodationName = accomodationDto.AccomodationName;
                accomodation.City = accomodationDto.City;
                accomodation.Type = accomodationDto.Type;
                accomodation.Description = accomodationDto.Description;
                accomodation.OfferedCatering = accomodationDto.OfferedCatering;
                accomodation.PrePayment = accomodationDto.PrePayment;
                accomodation.CancellationFee = accomodationDto.CancellationFee;

                _reservationSiteContext.SaveChanges();
                return StatusCodes.Status200OK;
            }
            throw new Exception("No accomodation owner was found with given ID");

        }

        public async Task<int> InactivateAccomodation(int id)
        {
            Accomodation accomodation = await _reservationSiteContext.Accomodations.FindAsync(id);
            if (accomodation != null)
            {
                accomodation.IsActive = false;

                _reservationSiteContext.SaveChanges();
                return StatusCodes.Status200OK;
            }
            throw new Exception("No accomodation owner was found with given ID");

        }

        public async Task<int> ActivateAccomodation(int id)
        {
            Accomodation accomodation = await _reservationSiteContext.Accomodations.FindAsync(id);
            if (accomodation != null)
            {
                accomodation.IsActive = true;

                _reservationSiteContext.SaveChanges();
                return StatusCodes.Status200OK;
            }
            throw new Exception("No accomodation owner was found with given ID");

        }

        public async Task<int> DeleteAccomodation(int id)
        {
            Accomodation accomodation = await _reservationSiteContext.Accomodations.FindAsync(id);
            if (accomodation != null)
            {
                _reservationSiteContext.Accomodations.Remove(accomodation);
                _reservationSiteContext.SaveChanges();
                return StatusCodes.Status200OK;
            }
            throw new Exception("No accomodation was found with given ID");
        }

        public Accomodation GetAccomodationByRoomId(int id)
        {
            Room room = _reservationSiteContext.Rooms.Find(id);
            Accomodation accomodation = _reservationSiteContext.Accomodations.Where(accomodation => accomodation.AccomodationId == room.AccomodationId).FirstOrDefault();
            return accomodation;
        }

    }
}
