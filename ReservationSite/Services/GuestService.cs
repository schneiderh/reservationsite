﻿using Microsoft.AspNetCore.Http;
using ReservationSite.DataAccessLayer;
using ReservationSite.Models;
using ReservationSite.Models.Data_Transfer_Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReservationSite.Services
{
    public class GuestService
    {
        private readonly ReservationSiteContext _reservationSiteContext;
        private readonly PasswordService _passwordService;

        public GuestService(ReservationSiteContext reservationSiteContext, PasswordService passwordService)
        {
            _reservationSiteContext = reservationSiteContext;
            _passwordService = passwordService;
        }

        public IEnumerable<Guest> GetAllGuestsOfAccomodation(int id)
        {
            List<Room> roomsAtAccomodations = _reservationSiteContext.Rooms.Where(room => room.AccomodationId == id).ToList();
            List<Reservation> reservationsAtAccomodation = new List<Reservation>();
            foreach(var room in roomsAtAccomodations)
            {
                Reservation reservation = _reservationSiteContext.Reservations.Find(room.RoomId);
                if(reservation == null)
                {
                    throw new Exception("Error fetching reservation");
                }
                reservationsAtAccomodation.Add(reservation);
            }

            List<Guest> guests = new List<Guest>();
            foreach(var reservation in reservationsAtAccomodation)
            {
                Guest guest = _reservationSiteContext.Guests.Find(reservation.GuestId);
                if(guest == null)
                {
                    throw new Exception("Error fetching guest");
                }
                guests.Add(guest);
            }

            return guests;
        }

        public async Task<Guest> GetGuestById(int id)
        {
            Guest guest = _reservationSiteContext.Guests.Find(id);
            if (guest != null)
            {
                return guest;
            }
            throw new Exception("No accomodation owner was found with given ID");
        }

        public async Task<int> AddGuest(GuestDto guestDto)
        {
            Guest guest = new Guest
                (
                    guestDto.Name,
                    guestDto.Email,
                    guestDto.PhoneNumber,
                    guestDto.Zip,
                    guestDto.City,
                    guestDto.Street,
                    guestDto.HouseNumber,
                    guestDto.Password
                );
            this.SetPassword(guest, guestDto.Password);
            await _reservationSiteContext.Guests.AddAsync(guest);
            await _reservationSiteContext.SaveChangesAsync();
            return StatusCodes.Status200OK;
        }

        public async Task<Guest> GetGuestByEmail(string email)
        {
            Guest guest = _reservationSiteContext.Guests.Where(guest => guest.Email == email).SingleOrDefault();
            return guest;
        }

        public async Task<int> EditGuestData(int id, GuestDto guestDto)
        {
            Guest guestToEdit = await _reservationSiteContext.Guests.FindAsync(id);
            if (guestToEdit != null)
            {
                guestToEdit.Email = guestDto.Email;
                guestToEdit.Name = guestDto.Name;
                guestToEdit.PhoneNumber = guestDto.PhoneNumber;
                guestToEdit.Zip = guestDto.Zip;
                guestToEdit.City = guestDto.City;
                guestToEdit.Street = guestDto.Street;
                guestToEdit.HouseNumber = guestDto.HouseNumber;
                _reservationSiteContext.SaveChanges();
                return StatusCodes.Status200OK;
            }
            throw new Exception("No guest was found with given ID");

        }

        public void SetPassword(Guest guest, string password)
        {
            Password pswd = _passwordService.HashPassword(password);
            guest.Password = pswd.PasswordHash;
            guest.Salt = pswd.Salt;

        }
    }
}
