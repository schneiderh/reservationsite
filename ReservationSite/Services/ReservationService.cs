﻿using Microsoft.AspNetCore.Http;
using ReservationSite.DataAccessLayer;
using ReservationSite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReservationSite.Services
{
    public class ReservationService
    {
        private readonly ReservationSiteContext _reservationSiteContext;
        private readonly GuestService _guestService;
        private readonly AccomodationService _accomodationService;
        private readonly RoomService _roomService;
        private readonly ReservedDateService _reservedDateService;

        public ReservationService(ReservationSiteContext reservationSiteContext, GuestService guestService, AccomodationService accomodationService, RoomService roomService, ReservedDateService reservedDateService)
        {
            _reservationSiteContext = reservationSiteContext;
            _guestService = guestService;
            _accomodationService = accomodationService;
            _roomService = roomService;
            _reservedDateService = reservedDateService;
        }

        /**
         * guestId - aki teszi a foglalást
         * roomId - melyiks zobát foglaljuk le (szállást ez alapján megkeressük majd)
         * startDate - foglalás kezdete
         * endDate - foglalás vége
        */
        public async Task<int> MakeReservation(int guestId, int guestCount, int roomId, DateTime startDate, DateTime endDate)
        {
            var room = _roomService.GetRoomById(roomId);
            if(room.Result.MaxPersons < guestCount)
            {
                throw new Exception("Too many guests for one room");
            }
            Accomodation accomodation = _accomodationService.GetAccomodationByRoomId(roomId);
            int stayLength = _reservedDateService.GetStayLengthInInt(startDate, endDate);
            int totalValue = room.Result.PricePerPerson * guestCount * stayLength;

            using var reservationTransaction = _reservationSiteContext.Database.BeginTransaction();
            try
            {
                if (!_reservedDateService.IsDateFree(startDate, endDate, roomId))
                {
                    throw new Exception("Given date is not free, please choose another date");
                }

                await _reservedDateService.ReserveRoomForInterval(roomId, startDate, endDate);

                Reservation reservation = new Reservation
                    (
                        startDate,
                        endDate,
                        accomodation.OfferedCatering,
                        totalValue,
                        totalValue * (float)(accomodation.PrePayment / 100),
                        totalValue * (float)(accomodation.CancellationFee / 100),
                        room.Result.RoomType,
                        guestId,
                        roomId
                    );
                _reservationSiteContext.Reservations.Add(reservation);
                _reservationSiteContext.SaveChanges();
                reservationTransaction.Commit();
                return StatusCodes.Status200OK;
            }
            catch (Exception)
            {
                throw new Exception("There was a problem making your reservation");
            }
            
        }

        public IEnumerable<Reservation> GetReservationsOfGuest(int id)
        {
            IEnumerable<Reservation> reservations = _reservationSiteContext.Reservations.Where(reservation => reservation.GuestId == id);
            return reservations;
        }

        public async Task<Reservation> GetReservationById(int id)
        {
            Reservation reservation = _reservationSiteContext.Reservations.Find(id);
            if(reservation != null)
            {
                return reservation;
            }
            throw new Exception("No reservation was found with current id");
        }

        public async Task<int> CancelReservation(int id)
        {
            Reservation reservation = _reservationSiteContext.Reservations.Find(id);
            if(reservation != null)
            {
                reservation.IsActive = false;
                if(reservation.Arrival.Date < DateTime.Now.Date)
                {
                    throw new Exception("Cancelling a past reservation is not possible");
                }
                await _reservedDateService.DeleteReservedDate(reservation.RoomId, reservation.Arrival, reservation.Departure);
                _reservationSiteContext.SaveChanges();
                return StatusCodes.Status200OK;
            }
            throw new Exception("No reservation was found with current id");
        }

        public async Task<int> EditReservationDates(int id, DateTime newStart, DateTime newEnd)
        {
            Reservation reservation = _reservationSiteContext.Reservations.Find(id);
            if(reservation.IsActive == false)
            {
                return StatusCodes.Status400BadRequest;
            }
            if(reservation != null)
            {
                if (_reservedDateService.IsDateFree(newStart, newEnd, reservation.RoomId))
                {
                    using var reservationEditTransaction = _reservationSiteContext.Database.BeginTransaction();
                    
                        await _reservedDateService.DeleteReservedDate(reservation.RoomId, reservation.Arrival, reservation.Departure);
                        await _reservedDateService.ReserveRoomForInterval(reservation.RoomId, newStart, newEnd);
                        reservation.Arrival = newStart;
                        reservation.Departure = newEnd;
                        _reservationSiteContext.SaveChanges();
                        reservationEditTransaction.Commit();
                        return StatusCodes.Status200OK;
                    
                }
                throw new Exception("New date is not free");
                
            }
            throw new Exception("No reservation was found with current id");
        }

        public IEnumerable<Reservation> GetAllReservationsOfRoom(int roomId)
        {
            IEnumerable<Reservation> reservations = _reservationSiteContext.Reservations.Where(reservation => reservation.RoomId == roomId);
            return reservations;
        }
    }
}
