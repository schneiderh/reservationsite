﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using ReservationSite.DataAccessLayer;
using ReservationSite.Enums;
using ReservationSite.Models;
using ReservationSite.Models.Data_Transfer_Objects;
using ReservationSite.Settings;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace ReservationSite.Services
{
    public class AuthorizationService
    {
        private readonly GuestService _guestService;
        private readonly AccomodationOwnerService _accomodationOwnerService;
        private readonly PasswordService _passwordService;
        private readonly JwtSettings _jwtSettings;
        private readonly ReservationSiteContext _reservationSiteContext;


        public AuthorizationService(GuestService guestService,
            AccomodationOwnerService accomodationOwnerService,
            PasswordService passwordService,
            IOptionsSnapshot<JwtSettings> jwtSettings,
            ReservationSiteContext reservationSiteContext
            )
        {
            _guestService = guestService;
            _accomodationOwnerService = accomodationOwnerService;
            _passwordService = passwordService;
            _jwtSettings = jwtSettings.Value;
            _reservationSiteContext = reservationSiteContext;
        }

        public string LogInUser(LoginDto loginDto)
        {
            if(loginDto.Role == Role.Guest)
            {
                var guest = _reservationSiteContext.Guests.Where(g => g.Email == loginDto.Email).First();
                if(_passwordService.VerifyPassword(loginDto.Password, guest.Password, guest.Salt))
                {
                    return this.GenerateJwt(guest) + "." + Role.Guest + "." + guest.Name + "." + guest.GuestId;
                }
                return StatusCodes.Status417ExpectationFailed.ToString();
            } else
            {
                AccomodationOwner accomodationOwner = _accomodationOwnerService.GetOwnerByEmail(loginDto.Email).Result;
                if (_passwordService.VerifyPassword(loginDto.Password, accomodationOwner.Password, accomodationOwner.Salt))
                {
                    return this.GenerateJwt(null, accomodationOwner) + "." + Role.Owner + "." + accomodationOwner.Name + "." +accomodationOwner.AccomodationOwnerId;
                }
                return StatusCodes.Status417ExpectationFailed.ToString();
            }
        }

        private string GenerateJwt(Guest guest = null, AccomodationOwner accomodationOwner = null)
        {
            List<Claim> claims = new List<Claim>();
            if(guest != null)
            {
                claims = new List<Claim>
                {
                    new Claim(JwtRegisteredClaimNames.Sub, guest.GuestId.ToString()),
                    new Claim(ClaimTypes.Name, guest.Name),
                    new Claim(ClaimTypes.Email, guest.Email),
                    new Claim(ClaimTypes.Role, Role.Guest.ToString()),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    new Claim(ClaimTypes.NameIdentifier, guest.GuestId.ToString())
                };
            } else
            {
                claims = new List<Claim>
                {
                    new Claim(JwtRegisteredClaimNames.Sub, accomodationOwner.AccomodationOwnerId.ToString()),
                    new Claim(ClaimTypes.Name, accomodationOwner.Name),
                    new Claim(ClaimTypes.Email, accomodationOwner.Email),
                    new Claim(ClaimTypes.Role, Role.Owner.ToString()),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    new Claim(ClaimTypes.NameIdentifier, accomodationOwner.AccomodationOwnerId.ToString())
                };
            }

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtSettings.Secret));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.Now.AddDays(Convert.ToDouble(_jwtSettings.ExpirationInDays));

            var token = new JwtSecurityToken(
                issuer: _jwtSettings.Issuer,
                audience: _jwtSettings.Issuer,
                claims,
                expires: expires,
                signingCredentials: creds
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
