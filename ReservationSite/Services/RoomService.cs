﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using ReservationSite.DataAccessLayer;
using ReservationSite.Models;
using ReservationSite.Models.Data_Transfer_Objects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ReservationSite.Services
{
    public class RoomService
    {
        private readonly ReservationSiteContext _reservationSiteContext;
        private readonly IWebHostEnvironment _hostingEnvironment;

        public RoomService(ReservationSiteContext reservationSiteContext, IWebHostEnvironment hostingEnvironment)
        {
            _reservationSiteContext = reservationSiteContext;
            _hostingEnvironment = hostingEnvironment;
        }

        public async Task<int> AddRoom(RoomDto roomDto)
        {
            string filePath = this.UploadImage(roomDto.Image); 
            Room room = new Room(
                    roomDto.RoomName,
                    roomDto.RoomType,
                    roomDto.MaxPersons,
                    roomDto.PricePerPerson,
                    filePath,
                    roomDto.AccomodationId
                );
            await _reservationSiteContext.Rooms.AddAsync(room);
            await _reservationSiteContext.SaveChangesAsync();
            return StatusCodes.Status200OK;
        }

        private string UploadImage(IFormFile image)
        {
            Guid id = Guid.NewGuid();
            string idString = id.ToString();
            var fileName = idString + Path.GetFileName(image.FileName);
            var filePath = Path.Combine("C:/Users/ELITEBOOK/Desktop/Szakdoga programozas/ReservationSiteFrontEnd/src/assets/images/", fileName);

            using (var fileStream = new FileStream(filePath, FileMode.Create))
            {
                image.CopyTo(fileStream);
            }

            filePath = "assets/images/" + fileName;
            return filePath;
        }

        public IEnumerable<Room> GetAllRoomsOfAccomodation(int id)
        {
            IEnumerable<Room> roomsOfAccomodation = _reservationSiteContext.Rooms.Where(room => room.AccomodationId == id);
            return roomsOfAccomodation;
        }

        public IEnumerable<Room> GetAllActiveRoomsOfAccomodation(int id)
        {
            IEnumerable<Room> activeRoomsOfAccomodation = _reservationSiteContext.Rooms.Where(room => room.AccomodationId == id && room.IsActive == true);
            return activeRoomsOfAccomodation;
        }

        public async Task<Room> GetRoomById(int id)
        {
            Room room = await _reservationSiteContext.Rooms.FindAsync(id);
            if(room != null)
            {
                return room;
            }
            return null;
        }

        public async Task<int> EditRoomDetails(int id, RoomDto roomDto, bool withImage)
        {
            Room room = await _reservationSiteContext.Rooms.FindAsync(id);

            string filePath = null;
            if (withImage)
            {
                filePath = this.UploadImage(roomDto.Image);
                System.IO.File.Delete("C:/Users/ELITEBOOK/Desktop/Szakdoga programozas/ReservationSiteFrontEnd/src/" + room.ImagePath);
            }

            if (room != null)
            {
                room.RoomName = roomDto.RoomName;
                room.RoomType = roomDto.RoomType;
                room.MaxPersons = roomDto.MaxPersons;
                room.PricePerPerson = roomDto.PricePerPerson;
                room.ImagePath = withImage ? filePath : room.ImagePath;

                _reservationSiteContext.SaveChanges();
                return StatusCodes.Status200OK;
            }
            throw new Exception("Room with given id was not found");

        }

        public async Task<int> DeleteRoom(int id)
        {
            Room room = await _reservationSiteContext.Rooms.FindAsync(id);
            if (room != null)
            {
                _reservationSiteContext.Rooms.Remove(room);
                _reservationSiteContext.SaveChanges();
                return StatusCodes.Status200OK;
            }
            throw new Exception("No room was found with given ID");
        }

        public async Task<int> InactivateRoom(int id)
        {
            Room room = await _reservationSiteContext.Rooms.FindAsync(id);
            if (room != null)
            {
                room.IsActive = false;
                //TODO: Lock every date for this room

                _reservationSiteContext.SaveChanges();
                return StatusCodes.Status200OK;
            }
            throw new Exception("No room was found with given ID");
        }

        public async Task<int> ActivateRoom(int id)
        {
            Room room = await _reservationSiteContext.Rooms.FindAsync(id);
            if (room != null)
            {
                room.IsActive = true;
                //TODO: Lock every date for this room

                _reservationSiteContext.SaveChanges();
                return StatusCodes.Status200OK;
            }
            throw new Exception("No room was found with given ID");
        }
    }
}
