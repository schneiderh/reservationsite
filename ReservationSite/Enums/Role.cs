﻿namespace ReservationSite.Enums
{
    public enum Role
    {
        Guest,
        Owner
    }
}
