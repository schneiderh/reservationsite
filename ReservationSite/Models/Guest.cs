﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ReservationSite.Enums;
using ReservationSite.Interfaces;
using ReservationSite.Services;

namespace ReservationSite.Models
{
    public class Guest
    {
        //Helping classes
        PasswordService _passwordService = new PasswordService();

        //FIELDS FOR COMPOSITE ATTRIBUTE
        private string _zip;
        private string _city;
        private string _street;
        private string _houseNumber;
        
        //NORMAL ATTRIBUTES
        public int GuestId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Password { get; set; }
        public string Salt { get; set; }

        //MAKING THE COMPOSITE ATTRIBUTE: ADRESS
        public string Zip
        {
            get { return _zip; }
            set
            {
                _zip = value;
                this.RefreshAdress();
            }
        }

        public string City
        {
            get { return _city; }
            set
            {
                _city = value;
                this.RefreshAdress();
            }
        }

        public string Street
        {
            get { return _street; }
            set
            {
                _street = value;
                this.RefreshAdress();
            }
        }

        public string HouseNumber
        {
            get { return _houseNumber; }
            set
            {
                _houseNumber = value;
                this.RefreshAdress();
            }
        }

        public string Adress { get; private set; }

        //PROPERTIES BECAUSE OF RELATIONSHIPS
        public List<Reservation> Reservations { get; set; }
        public Role Role { get; set; }

        //CTOR
        public Guest(string name, string email, string phoneNumber, string zip, string city, string street, string houseNumber, string password = null)
        {
            Name = name;
            Email = email;
            Password = password;
            PhoneNumber = phoneNumber;
            Zip = zip;
            City = city;
            Street = street;
            HouseNumber = houseNumber;
            Role = Role.Guest;
        }


        private void RefreshAdress()
        {
            Adress = _zip + " " + _city + ", " + _street + " " + _houseNumber;
        }


    }
}
