﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ReservationSite.Models
{
    public class Room
    {
        public int RoomId { get; set; }
        public string RoomName { get; set; }
        public string RoomType { get; set; }
        public int MaxPersons { get; set; }
        public int PricePerPerson { get; set; }
        public string ImagePath { get; set; }
        public bool IsActive { get; set; }
        public int AccomodationId { get; set; }
        public List<ReservedDate> ReservedDays { get; set; }
        public Accomodation Accomodation { get; set; }
        public List<Reservation> Reservations { get; set; }

        public Room(string roomName, string roomType, int maxPersons, int pricePerPerson, string imagePath, int accomodationId)
        {
            RoomName = roomName;
            RoomType = roomType;
            MaxPersons = maxPersons;
            PricePerPerson = pricePerPerson;
            ImagePath = imagePath;
            AccomodationId = accomodationId;
            IsActive = true;
        }
    }
}
