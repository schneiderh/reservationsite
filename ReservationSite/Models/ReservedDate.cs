﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReservationSite.Models
{
    public class ReservedDate
    {
        public int ReservedDateId { get; set; }
        public DateTime Date { get; set; }
        public int RoomId { get; set; }
        public Room Room { get; set; }

        public ReservedDate(DateTime date, int roomId)
        {
            Date = date;
            RoomId = roomId;
        }

    }
}
