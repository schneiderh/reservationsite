﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReservationSite.Models
{
    public class Reservation
    {
        public int Id { get; set; }
        public DateTime Arrival { get; set; }
        public DateTime Departure { get; set; }
        public bool IsActive { get; set; }
        public string Catering { get; set; }
        public int TotalValue { get; set; }
        public float PrePayment { get; set; }
        public float CancellationFee { get; set; }
        public string RoomType { get; set; }
        public int GuestId { get; set; }
        public Guest Guest { get; set; }
        public int RoomId { get; set; }
        public Room Room { get; set; }
        public Review Reviews { get; set; }

        public Reservation(DateTime arrival, DateTime departure, string catering, int totalValue, float prePayment, float cancellationFee, string roomType, int guestId, int roomId)
        {
            Arrival = arrival;
            Departure = departure;
            Catering = catering;
            TotalValue = totalValue;
            PrePayment = prePayment;
            CancellationFee = cancellationFee;
            RoomType = roomType;
            GuestId = guestId;
            RoomId = roomId;
            IsActive = true;
        }

    }
}
