﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReservationSite.Models
{
    public class Password
    {
        public string PasswordHash { get; set; }
        public string Salt { get; set; }

        public Password(string passwordHash, string salt)
        {
            PasswordHash = passwordHash;
            Salt = salt;
        }

    }
}
