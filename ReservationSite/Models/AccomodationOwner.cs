﻿using ReservationSite.Enums;
using ReservationSite.Interfaces;
using ReservationSite.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReservationSite.Models
{
    public class AccomodationOwner
    {
        //Helping classes
        PasswordService _passwordService = new PasswordService();


        public int AccomodationOwnerId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Password { get; set; }
        public string Salt { get; set; }
        public List<Accomodation> Accomodations { get; set; }
        public Role Role { get; set; }

        public AccomodationOwner(string name, string email, string phoneNumber, string password = null)
        {
            Name = name;
            Email = email;
            PhoneNumber = phoneNumber;
            Password = password;
            Role = Role.Owner;
        }
    }
}
