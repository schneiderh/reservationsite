﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReservationSite.Models.Data_Transfer_Objects
{
    public class ReviewDto
    {
        public string WrittenReview { get; set; }

        public float Score { get; set; }
        public int ReservationId { get; set; }

        public ReviewDto(string writtenReview, float score, int reservationId)
        {
            WrittenReview = writtenReview;
            Score = score;
            ReservationId = reservationId;
        }

    }
}
