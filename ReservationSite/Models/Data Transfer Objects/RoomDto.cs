﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReservationSite.Models.Data_Transfer_Objects
{
    public class RoomDto
    {
        public int RoomId { get; set; }
        public string RoomName { get; set; }
        public string RoomType { get; set; }
        public int MaxPersons { get; set; }
        public int PricePerPerson { get; set; }
        public IFormFile Image { get; set; }
        public int AccomodationId { get; set; }

    }
}
