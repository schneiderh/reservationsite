﻿using ReservationSite.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReservationSite.Models.Data_Transfer_Objects
{
    public class LoginDto
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public Role Role { get; set; }

        public LoginDto(string email, string password, Role role)
        {
            Email = email;
            Password = password;
            Role = role;
        }
    }
}
