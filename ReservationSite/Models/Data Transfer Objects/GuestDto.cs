﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReservationSite.Models.Data_Transfer_Objects
{
    public class GuestDto
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string PhoneNumber { get; set; }
        public string Zip { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string HouseNumber { get; set; }

        public GuestDto(string name, string email, string password, string phoneNumber, string zip, string city, string street, string houseNumber)
        {
            Name = name;
            Email = email;
            Password = password;
            PhoneNumber = phoneNumber;
            Zip = zip;
            City = city;
            Street = street;
            HouseNumber = houseNumber;
        }
    }
}
