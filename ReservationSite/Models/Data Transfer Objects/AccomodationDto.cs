﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReservationSite.Models.Data_Transfer_Objects
{
    public class AccomodationDto
    {
        public string AccomodationName { get; set; }
        public string City { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public string OfferedCatering { get; set; }
        public float PrePayment { get; set; }
        public float CancellationFee { get; set; }
        public int AccomodationOwnerId { get; set; }

        public AccomodationDto(string accomodationName, string city, string type, string description, string offeredCatering, float prePayment, float cancellationFee, int accomodationOwnerId)
        {
            AccomodationName = accomodationName;
            City = city;
            Type = type;
            Description = description;
            OfferedCatering = offeredCatering;
            PrePayment = prePayment;
            CancellationFee = cancellationFee;
            AccomodationOwnerId = accomodationOwnerId;
        }
    }
}
